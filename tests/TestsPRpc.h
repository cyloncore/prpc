#include <QObject>

class TestsPRpc : public QObject
{
  Q_OBJECT
public:
  explicit TestsPRpc(QObject* parent = 0);  
private slots:
  void testMessageHeader();
  void testTCPConnection();
  void testRequest();
  void testStream();
};
