#include "TestsPRpc.h"

#include <pRpc/Connection.h>
#include <pRpc/AbstractService.h>
#include <pRpc/Answer.h>
#include <pRpc/Errors.h>
#include <pRpc/ErrorCode.h>
#include <pRpc/MessageHeader.h>
#include <pRpc/NoAuthentication.h>
#include <pRpc/Peer.h>
#include <pRpc/PeerInfo.h>
#include <pRpc/TcpTransport.h>

#include <QtTest>

TestsPRpc::TestsPRpc(QObject* parent): QObject(parent)
{

}

void TestsPRpc::testMessageHeader()
{
  pRpc::MessageHeader mh;
  QVERIFY(not mh.isValid());
  const quint64 size = 12314;
  const quint16 type = 1;
  
  mh = pRpc::MessageHeader(size, type);
  QVERIFY(mh.isValid());
  QCOMPARE(mh.type(), type);
  QCOMPARE(mh.size(), size);
  
  QByteArray data;
  {
    QBuffer buffer;
    buffer.open(QIODevice::WriteOnly);
    QDataStream stream(&buffer);
    stream << mh;
    data = buffer.buffer();
  }
  QCOMPARE(data.size(), int(pRpc::MessageHeader::F_END));
  
  pRpc::MessageHeader mh2;
  {
    QBuffer buffer(&data);
    buffer.open(QIODevice::ReadOnly);
    QDataStream stream(&buffer);
    stream >> mh2;
  }
  QVERIFY(mh2.isValid());
  QCOMPARE(mh2.size(), size);
  QCOMPARE(mh2.type(), type);
}

struct TestService : public pRpc::AbstractService
{
  void handleData(pRpc::Connection _remoteConnection, int _id, const QByteArray& _data) override
  {
    Q_UNUSED(_id);
    Q_UNUSED(_data);
  }
  QVariant handleRequest(pRpc::Connection _remoteConnection, const QString& _method, const QVariantList& _arguments) override
  {
    if(_method == "add")
    {
      if(_arguments.size() == 2)
      {
        return _arguments[0].toInt() + _arguments[1].toInt();
      } else {
        return QVariant::fromValue(pRpc::Errors::InvalidArgumentsCount);
      }
    }
    return pRpc::AbstractService::handleRequest(_remoteConnection, _method, _arguments);
  }
  void handleStream(pRpc::Connection _remoteConnection, int _id, QIODevice* _iodevice) override
  {
    while(_iodevice->bytesAvailable() != _iodevice->size())
    {
      _iodevice->waitForReadyRead(10000);
    }
    QByteArray array = _iodevice->readAll();
    streamsResult[_id] = array;
    
  }
  QHash<int, QByteArray> streamsResult;
};

void TestsPRpc::testTCPConnection()
{
  pRpc::TcpTransport* t1 = new pRpc::TcpTransport;
  pRpc::TcpTransport* t2 = new pRpc::TcpTransport;
  
  QVERIFY(t1->listen(QHostAddress::Any, 10000));
  QVERIFY(t2->listen(QHostAddress::Any, 12000));
  
  pRpc::Peer p1("p1", "p1", t1, new pRpc::NoAuthentication);
  p1.addService("service_a", new TestService);
  pRpc::Peer p2("p2", "p2", t2, new pRpc::NoAuthentication);
  p2.addService("service_b", new TestService);
  
  pRpc::Connection connection1 = p1.connectTo("localhost:12000");
  QVERIFY(connection1.isValid());
  pRpc::Connection connection2 = p2.connectTo("127.0.0.1:10000");
  QVERIFY(connection2.isValid());
  
  for(int i = 0; i < 10; ++i)
  {
    QThread::sleep(1);
    QCoreApplication::processEvents();
    if(connection1.status() == pRpc::Connection::Status::Connected and connection2.status() == pRpc::Connection::Status::Connected) break;
    QVERIFY(connection1.status() != pRpc::Connection::Status::Disconnected);
    QVERIFY(connection2.status() != pRpc::Connection::Status::Disconnected);
  }
  QVERIFY(connection1.status() == pRpc::Connection::Status::Connected);
  QVERIFY(connection2.status() == pRpc::Connection::Status::Connected);
  
  QCOMPARE(connection1.remotePeerInfo().name(), QString("p2"));
  QCOMPARE(connection1.remotePeerInfo().address(), QString("::1:12000"));
  QCOMPARE(connection2.remotePeerInfo().name(), QString("p1"));
  QCOMPARE(connection2.remotePeerInfo().address(), QString("127.0.0.1:10000"));
}

void TestsPRpc::testRequest()
{
  pRpc::TcpTransport* t1 = new pRpc::TcpTransport;
  pRpc::TcpTransport* t2 = new pRpc::TcpTransport;
  
  QVERIFY(t2->listen(QHostAddress::Any, 12000));
  
  pRpc::Peer p1("p1", "p1", t1, new pRpc::NoAuthentication);
  p1.addService("service_a", new TestService);
  pRpc::Peer p2("p2", "p2", t2, new pRpc::NoAuthentication);
  p2.addService("service_b", new TestService);
  
  pRpc::Connection connection1 = p1.connectTo("localhost:12000");
  QVERIFY(connection1.isValid());
  
  for(int i = 0; i < 10; ++i)
  {
    QThread::sleep(1);
    QCoreApplication::processEvents();
    if(connection1.status() == pRpc::Connection::Status::Connected) break;
  }
  
  // Test unknown method
  pRpc::Answer a = p1.request(connection1, "service_b", "no method", QVariantList());
  a.waitForAvailable(10000);
  QVERIFY(a.isAvailable());
  QVERIFY(not a.isSuccessful());
  QVERIFY(a.result().canConvert<pRpc::Error>());
  pRpc::Error err = qvariant_cast<pRpc::Error>(a.result());
  QCOMPARE(err.code(), int(pRpc::EC_UNKNOWN_METHOD));
  QCOMPARE(err.message(), pRpc::Errors::UnknownMethod.message());

  // Test unknown service
  a = p1.request(connection1, "service_a", "no method", QVariantList());
  a.waitForAvailable(10000);
  QVERIFY(a.isAvailable());
  QVERIFY(not a.isSuccessful());
  QVERIFY(a.result().canConvert<pRpc::Error>());
  err = qvariant_cast<pRpc::Error>(a.result());
  QCOMPARE(err.code(), int(pRpc::EC_UNKNOWN_SERVICE));
  QCOMPARE(err.message(), pRpc::Errors::UnknownService.message());

  // Test method wrong arguments
  QVariantList var;
  var.push_back(1);
  
  a = p1.request(connection1, "service_b", "add", var);
  a.waitForAvailable(10000);
  QVERIFY(a.isAvailable());
  QVERIFY(not a.isSuccessful());
  QVERIFY(a.result().canConvert<pRpc::Error>());
  err = qvariant_cast<pRpc::Error>(a.result());
  QCOMPARE(err.code(), int(pRpc::EC_INVALID_ARGUMENTS_COUNT));
  QCOMPARE(err.message(), pRpc::Errors::InvalidArgumentsCount.message());

  // Test method  
  var.push_back(2);
  a = p1.request(connection1, "service_b", "add", var);
  a.waitForAvailable(10000);
  QVERIFY(a.isAvailable());
  QVERIFY(a.isSuccessful());
  bool test_convert;
  QCOMPARE(a.result().toInt(&test_convert), 3);  
  QVERIFY(test_convert);
}

void TestsPRpc::testStream()
{
  pRpc::TcpTransport* t1 = new pRpc::TcpTransport;
  pRpc::TcpTransport* t2 = new pRpc::TcpTransport;
  
  QVERIFY(t2->listen(QHostAddress::Any, 12000));
  
  pRpc::Peer p1("p1", "p1", t1, new pRpc::NoAuthentication);
  p1.addService("service_a", new TestService);
  pRpc::Peer p2("p2", "p2", t2, new pRpc::NoAuthentication);
  TestService* service_b = new TestService;
  p2.addService("service_b", service_b);
  
  pRpc::Connection connection1 = p1.connectTo("localhost:12000");
  QVERIFY(connection1.isValid());
  
  for(int i = 0; i < 10; ++i)
  {
    QThread::sleep(1);
    QCoreApplication::processEvents();
    if(connection1.status() == pRpc::Connection::Status::Connected) break;
  }
  QVERIFY(connection1.status() == pRpc::Connection::Status::Connected);
  QByteArray data(10000, Qt::Uninitialized);
  for(int i = 0; i < data.size(); ++i)
  {
    data[i] = rand();
  }
  QBuffer* buff = new QBuffer(&data);
  
  p1.stream(connection1, "service_b", 1, buff);
  
  while(service_b->streamsResult.size() != 1)
  {
    QCoreApplication::processEvents();
    QThread::sleep(1);
  }
  QVERIFY(service_b->streamsResult.contains(1));
  QByteArray result = service_b->streamsResult.value(1);
  QCOMPARE(result.size(), data.size());
  QCOMPARE(result, data);
}

QTEST_MAIN(TestsPRpc)
