#include "TypesManager.h"

#include <QHash>
#include <QString>

#include "Type.h"

struct TypesManager::Private
{
  QHash<QString, const Type*> types;
  QList<const Type*> types_list;
};

TypesManager::TypesManager() : d(new Private)
{

}

TypesManager::~TypesManager()
{
  delete d;
}

#define BT(_NAME_, _CPP_NAME_) addType(new Type(_NAME_, _CPP_NAME_, _CPP_NAME_));
#define BTT(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_) addType(new Type(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_));

void TypesManager::initBuiltinTypes()
{
  BT ("boolean", "bool");
  BT ("integer", "int");
  BT ("integer64", "qint64");
  BT ("double", "double");
  BTT ("empty", "::pRpc::Empty", "const ::pRpc::Empty&");
  BTT("bytes",  "QByteArray", "const QByteArray&")
  BTT("string",  "QString", "const QString&")
  BTT("datetime",  "QDateTime", "const QDateTime&")
}

bool TypesManager::addType(const Type* _type)
{
  if(d->types.contains(_type->name()))
  {
    return false;
  }
  d->types[_type->name()] = _type;
  d->types_list.append(_type);
  return true;
}

const Type* TypesManager::type(const QString& _name)
{
  return d->types.value(_name, 0);
}

QList< const Type* > TypesManager::types() const
{
  return d->types_list;
}
