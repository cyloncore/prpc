#include <QList>

class QString;
class Type;

class TypesManager
{
public:
  TypesManager();
  ~TypesManager();
  void initBuiltinTypes();
    bool addType(const Type* _type);
  const Type* type(const QString& _name);
  QList<const Type*> types() const;
private:
  struct Private;
  Private* const d;
};
