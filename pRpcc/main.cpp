#include <iostream>

#include <QDebug>
#include <QFile>

#include "Error.h"
#include "Generator.h"
#include "Lexer.h"
#include "Parser.h"
#include "TypesManager.h"

int main(int _argc, char** _argv)
{
  if(_argc < 3)
  {
    qWarning() << "pRpcc (-qml) [input.rpd] [output]";
    return -1;
  }
  QString input_filename, output_filename;
  bool has_qml = false;
  if(QString(_argv[1]) == "-qml")
  {
    has_qml         = true;
    input_filename  = _argv[2];
    output_filename = _argv[3];
  } else {
    input_filename  = _argv[1];
    output_filename = _argv[2];
  }
  
  QFile file(input_filename);
  file.open(QIODevice::ReadOnly);
  
  Lexer lexer(&file);
  TypesManager typesManager;
  typesManager.initBuiltinTypes();
  Parser parser(&lexer, &typesManager, input_filename);
  QList<ServiceDefinition*> definition = parser.parse();
  
  if(not definition.empty())
  {
    Generator gen;
    gen.generate(parser.namespaces(), &typesManager, definition, output_filename, has_qml);
    return 0;
  } else {
    QList<Error> errors = parser.errors();
    std::cout << "Compilation failed, with " << errors.size() << " error(s): " << std::endl;
    foreach(const Error& error, errors)
    {
      std::cout << qPrintable(error.filename) << "(" << error.line << "," << error.column << "): " << qPrintable(error.message) << std::endl;
    }
    return -1;
  }
  
  return -1;
}
