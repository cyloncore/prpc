#include <QString>

class Parser;

class Type
{
  friend class Parser;
public:
  struct Field
  {
    const Type* type;
    QString name;
  };
  enum TType
  {
    BUILT_IN, STRUCT, LIST
  };
public:
  Type(const QString& _name, const QString& _namespace);
  Type(const QString& _name, const QString& _cppName, const QString& _cppArgumentName);
  Type(const Type* _nested);
  ~Type();
  TType type() const;
  QString name() const;
  QString namespace_() const;
  QString fullCppName() const;
  QString cppName() const;
  QString cppArgumentName() const ;
  QList<Field> fields() const;
  const Type* listOf() const;
private:
  void addField(const Type* _type, const QString& _name);
private:
  struct Private;
  Private* const d;
};
