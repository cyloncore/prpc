#pragma once

#include <QDebug>
#include <QJSValue>
#include <QObject>

#include <pRpc/Answer.h>
#include <pRpc/Connection.h>
#include <pRpc/Error.h>
#include <pRpc/Peer.h>


/%
for(ServiceDefinition* definition : _definitions)
{
  for(const QString& ns : definition->namespaces())
  {
  %/
  namespace /%= ns %/
  {/%
  }
  %/
    namespace /%= definition->name() %/
    {
      inline QVariant toVariant(const QVariant& _variant)
      {
        if(_variant.userType() == qMetaTypeId<QJSValue>())
        {
          return _variant.value<QJSValue>().toVariant();
        } else {
          return _variant;
        }
      }
        /%
        for(const ServiceDefinition::Call& call : definition->calls())
        {
          QString class_name = call.name + "Caller";
          class_name[0] = class_name[0].toUpper();
          %/
      class /%= class_name %/ : public QObject
      {
        Q_OBJECT
      public:
        Q_PROPERTY(::pRpc::Peer* peer MEMBER m_peer NOTIFY peerChanged)
        Q_PROPERTY(::pRpc::Connection connection READ connection WRITE setConnection NOTIFY connectionChanged)
        Q_PROPERTY(QVariant answer READ answer NOTIFY answerChanged)
      public:
        /%= class_name %/() {}
        ~/%= class_name %/() {}
        Q_INVOKABLE void call(/%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              if(i != 0)
              {
              %/, /%
              }
              %/const QVariant& /%= call.arguments[i].name %//%
            }
            %/)
        {
          if(m_peer and m_connection.isValid())
          {
            QVariantList arguments;
            /%
            for(const ServiceDefinition::Call::Argument& arg : call.arguments)
            {
              %/
              arguments << toVariant(/%= arg.name %/);/%
            }
            %/
            m_peer->request(m_connection, "/%= definition->name() %/", "/%= call.name %/", arguments).executeOnAvailable([this,arguments](bool _s, const QVariant& _var)
            {
              if(_s)
              {
                QMetaObject::invokeMethod(this, "setAnswer", Qt::QueuedConnection, Q_ARG(QVariant, _var));
              } else {
                qWarning() << "Failed to get an answer when calling '/%= definition->name() %/' with error: " << _var.value<::pRpc::Error>().message() << " called with: " << arguments;
                emit(failure());
              }
            }
            );
          } else {
            if(not m_peer) qWarning() << "Peer is not set!";
            if(not m_connection.isValid()) qWarning() << "Not connected!";
            emit(failure());
          }
        }
        ::pRpc::Connection connection() const { return m_connection; }
        void setConnection(const ::pRpc::Connection& _conn) { m_connection = _conn; emit(connectionChanged()); }
      signals:
        void peerChanged();
        void connectionChanged();
        void answerChanged();
        void failure();
      private:
        inline QVariant answer() const { return m_answer; }
        ::pRpc::Peer* m_peer = nullptr;
        ::pRpc::Connection m_connection;
        Q_INVOKABLE void setAnswer(const QVariant& _answer) { m_answer = _answer; emit(answerChanged()); }
        QVariant m_answer;
      };
        /%
        }
        %/
      
      
    }
  /%
  for(const QString& ns : definition->namespaces())
  {
    Q_UNUSED(ns);
  %/
  }/%
  }
}
%/

