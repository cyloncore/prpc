#pragma once

/%
for(const QString& ns : _namespaces)
{
  %/
namespace /%= ns %/
{/%
}%/
  namespace Qml
  {
    void initialise();
  }
/%
for(const QString& ns : _namespaces)
{
  %/
}/%
}%/
