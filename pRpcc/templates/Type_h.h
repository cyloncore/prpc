/%
if(type->type() == Type::STRUCT)
{
  %/
  struct /%= type->name() %/
  {/%
    for(const Type::Field& field : type->fields())
    {
      %/
    /%= field.type->cppName() %/ /%= field.name %/;/%
    }
    %/
    QVariant toVariant() const;
    static /%= type->name() %/ fromVariant(const QVariant& _variant);
    static QList</%= type->name() %/> fromVariant(const QVariantList& _variant);
  };
  /%
}

