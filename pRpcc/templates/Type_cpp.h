/%
if(type->type() == Type::STRUCT)
{
  %/
  QVariant /%= type->name() %/::toVariant() const
  {
    QMap<QString, QVariant> var;
    
    /%
    foreach(const Type::Field& field, type->fields())
    {
      %/
      var["/%= field.name %/"] = VariantMarshall< /%= field.type->cppName() %/ >::to_variant(/%= field.name %/);
      /%
    }
    %/
    
    return var;
  }
  /%= type->name() %/ /%= type->name() %/::fromVariant(const QVariant& _variant)
  {
    QMap<QString, QVariant> hash = _variant.toMap();
    /%= type->name() %/ obj;
    
    /%
    foreach(const Type::Field& field, type->fields())
    {
      %/
      obj./%= field.name %/ = VariantMarshall< /%= field.type->cppName() %/ >::from_variant(hash.value("/%= field.name %/"));
      /%
    }
    %/
    
    return obj;
  }
  QList</%= type->name() %/> /%= type->name() %/::fromVariant(const QVariantList& _variant)
  {
    QList</%= type->name() %/> results;
    foreach(const QVariant& variant, _variant)
    {
      results.append(fromVariant(variant));
    }
    return results;
  }
  /%
}
%/
