/%
if(type->type() == Type::STRUCT)
{
  %/
  template<>
  struct VariantMarshall</%= fullname %/>
  {
    inline static QVariant to_variant(const /%= fullname %/& _t)
    {
      return _t.toVariant();
    }

    inline static /%= fullname %/ from_variant(const QVariant& _variant)
    {
      return /%= fullname %/::fromVariant(_variant);
    }

    static bool is_strict(const QVariant& _variant)
    {
      // TODO should check that the field are strictly all there
      return _variant.type() == qMetaTypeId< QMap<QString, QVariant> >();
    }
    
    static bool can_convert(const QVariant& _variant)
    {
      // TODO should check that the field are all there
      return _variant.canConvert< QMap<QString, QVariant> >();
    }
  };

  /%
}
%/
