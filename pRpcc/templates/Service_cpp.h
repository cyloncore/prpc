#include "/%= _output %/.h"

#include <pRpc/Peer.h>

// Marshaller
namespace
{
  template<typename _T_>
  struct VariantMarshall
  {
    inline static QVariant to_variant(const _T_& _t)
    {
      return QVariant::fromValue<_T_>(_t);
    }

    inline static _T_ from_variant(const QVariant& _variant)
    {
      return qvariant_cast<_T_>(_variant);
    }

    inline static bool is_strict(const QVariant& _variant)
    {
      return _variant.type() == qMetaTypeId<_T_>();
    }
    
    inline static bool can_convert(const QVariant& _variant)
    {
      return _variant.canConvert<_T_>();
    }
  };

  template<typename _T_>
  struct VariantMarshall< QList<_T_> >
  {
    inline static QVariant to_variant(const QList<_T_>& _t)
    {
      QVariantList list;
      foreach(const _T_& t, _t)
      {
        list << VariantMarshall<_T_>::to_variant(t);
      }
      return list;
    }
    inline static QList<_T_> from_variant(const QVariant& _variant)
    {
      QVariantList list = _variant.toList();
      QList<_T_> result;
      foreach(const QVariant& var, list)
      {
        result.push_back(VariantMarshall<_T_>::from_variant(var));
      }
      return result;
    }
    inline static bool is_strict(const QVariant& _variant)
    {
      if(_variant.type() != qMetaTypeId< QVariantList >()) return false;
      QVariantList list = _variant.toList();
      foreach(const QVariant& var, list)
      {
        if(not VariantMarshall<_T_>::is_strict(var)) return false;
      }
      return true;
    }
    
    inline static bool can_convert(const QVariant& _variant)
    {
      if(not _variant.canConvert< QVariantList >()) return false;
      QVariantList list = _variant.toList();
      foreach(const QVariant& var, list)
      {
        if(not VariantMarshall<_T_>::can_convert(var)) return false;
      }
      return true;
    }
  };
  template<>
  struct VariantMarshall<pRpc::Empty>
  {
    inline static QVariant to_variant(const pRpc::Empty& )
    {
      return QVariant();
    }

    inline static pRpc::Empty from_variant(const QVariant& _variant)
    {
      return pRpc::Empty();
    }

    inline static bool is_strict(const QVariant& _variant)
    {
      return _variant.type() == QVariant::Invalid;
    }
    
    inline static bool can_convert(const QVariant& _variant)
    {
      return true;
    }
  };
  template<>
  struct VariantMarshall<QByteArray>
  {
    inline static QVariant to_variant(const QByteArray& _array)
    {
      return QVariant(QString::fromLatin1(_array.toBase64()));
    }

    inline static QByteArray from_variant(const QVariant& _variant)
    {
      return QByteArray::fromBase64(_variant.toString().toLatin1());
    }

    inline static bool is_strict(const QVariant& _variant)
    {
      return _variant.type() == QVariant::String;
    }
    
    inline static bool can_convert(const QVariant& _variant)
    {
      return _variant.canConvert<QString>();
    }
  };
}

namespace
{
/%
for(const Type* type : _typesManager->types())
{
  QString fullname = QString("%1::%2").arg(_namespaces.join("::")).arg(type->name());
#include "Type_VariantMarshall_cpp.h"
}
%/
}
/%
for(const QString& ns : _namespaces)
{
  %/
namespace /%= ns %/
{/%
}
  QStringList generated_return_types;
  for(ServiceDefinition* definition : _definitions)
  {
    for(const ServiceDefinition::Call& call : definition->calls())
    {
      QString type_name = call.returnType->fullCppName();
      if(not generated_return_types.contains(type_name))
      {
        %/
  template<>
  void Answer</%= type_name %/>::executeOnAvailable(const std::function<void(bool, const /%= type_name %/&)>& _function)
  {
    m_answer.executeOnAvailable([_function](bool _s, const QVariant& _v)
    {
      _function(_s, VariantMarshall</%= type_name %/>::from_variant(_v));
    });
  }
  template<>
  /%= type_name %/ Answer</%= type_name %/>::result() const
  {
    return VariantMarshall</%= type_name %/>::from_variant(m_answer.result());
  }
  /%
        generated_return_types.append(type_name);
      }
    }
  }
  for(const Type* type : _typesManager->types())
  {
#include "Type_cpp.h"
  }
for(const QString& ns : _namespaces)
{
  Q_UNUSED(ns);
  %/
}/%
}

for(ServiceDefinition* definition : _definitions)
{
  %/
  namespace
  {
    /%
    foreach(const Type* type, definition->typesManager()->types())
    {
      QString fullname = QString("%1::%2::%3").arg(definition->namespaces().join("::")).arg(definition->name()).arg(type->name());
#include "Type_VariantMarshall_cpp.h"
    }
    %/
  }
  /%
  for(const QString& ns : definition->namespaces())
  {
  %/
  namespace /%= ns %/
  {/%
  }
  %/
    namespace /%= definition->name() %/
    {
      /%
      foreach(const Type* type, definition->typesManager()->types())
      {
#include "Type_cpp.h"
      }
      %/
      
      struct Caller::Private
      {
        ::pRpc::Peer* peer;
        ::pRpc::Connection connection;
      };
      Caller::Caller(::pRpc::Peer* _peer, ::pRpc::Connection _connection) : d(new Private)
      {
        d->peer       = _peer;
        d->connection = _connection;
      }
      Caller::~Caller()
      {
        delete d;
      }

      /%
      for(const ServiceDefinition::Call& call : definition->calls())
      {
        %/
        Answer</%= call.returnType->cppName() %/> Caller::/%= call.name %/(
          /%
          for(int i = 0; i < call.arguments.size(); ++i)
          {
            if(i != 0)
            {
            %/
              ,
            /%
            }
            %/
            /%= call.arguments[i].type->cppArgumentName() %/ /%= call.arguments[i].name %/
            /%
          }
          %/
          )
        {
          QVariantList arguments;
          /%
          foreach(const ServiceDefinition::Call::Argument& arg, call.arguments)
          {
            %/
            arguments << VariantMarshall< /%= arg.type->cppName() %/ >::to_variant(/%= arg.name %/);
            /%
          }
          %/
          return d->peer->request(d->connection, "/%= definition->name() %/", "/%= call.name %/", arguments);
        }
        /%
      }
      %/
      
      Service::Service()
      {
        
      }
      Service::~Service()
      {
        
      }
      QVariant Service::handleRequest(::pRpc::Connection _connection, const QString& _method, const QVariantList& _arguments)
      {
        /%
        foreach(const ServiceDefinition::Call& call, definition->calls())
        {
          %/
          if(_method == "/%= call.name %/" and _arguments.size() == /%= call.arguments.size() %/
            /%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              const ServiceDefinition::Call::Argument& argument = call.arguments[i];
              %/
              and VariantMarshall< /%= argument.type->cppName()%/ >::is_strict(_arguments[/%= i %/])/%
            }
            %/
          )
          {
            return VariantMarshall< /%= call.returnType->cppName() %/ >::to_variant(/%= call.name %/(_connection/%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              const ServiceDefinition::Call::Argument& argument = call.arguments[i];
              %/, VariantMarshall< /%= argument.type->cppName()%/ >::from_variant(_arguments[/%= i %/])/%
            }
            %/));
          }
          /%
        }
        // Support for overload, the next loop is less strict and take the first that can convert
        foreach(const ServiceDefinition::Call& call, definition->calls())
        {
          %/
          if(_method == "/%= call.name %/" and _arguments.size() == /%= call.arguments.size() %//%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              const ServiceDefinition::Call::Argument& argument = call.arguments[i];
              %/
              and VariantMarshall< /%= argument.type->cppName()%/ >::can_convert(_arguments[/%= i %/])/%
            }
            %/)
          {
            return VariantMarshall< /%= call.returnType->cppName() %/ >::to_variant(/%= call.name %/(_connection/%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              const ServiceDefinition::Call::Argument& argument = call.arguments[i];
              %/, VariantMarshall< /%= argument.type->cppName()%/ >::from_variant(_arguments[/%= i %/])/%
            }
            %/));
          }
          /%
        }
        %/
        return AbstractService::handleRequest(_connection, _method, _arguments);
      }
    }
  /%
  for(const QString& ns : definition->namespaces())
  {
    Q_UNUSED(ns);
  %/
  }/%
  }
}
%/
