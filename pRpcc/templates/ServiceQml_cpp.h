#include "/%= _output %/Qml_p.h"

#include <QQmlEngine>

/%
for(const QString& ns : _namespaces)
{
  %/
namespace /%= ns %/
{/%
}%/
  namespace Qml
  {
    void initialise()
    {
      qRegisterMetaType<::pRpc::Peer*>("::pRpc::Peer*");
      qRegisterMetaType<::pRpc::Connection>("::pRpc::Connection");
/%
for(ServiceDefinition* definition : _definitions)
{
  QString ns = definition->namespaces().join("::") + "::" + definition->name() + "::";
  QString uri = definition->namespaces().join(".") + "." + definition->name();
  for(const ServiceDefinition::Call& call : definition->calls())
  {
    QString class_name = call.name + "Caller";
    class_name[0] = class_name[0].toUpper();
    %/
      qmlRegisterType</%= ns %//%= class_name %/>("/%= uri %/", 1, 0, "/%= class_name %/");
  /%
  }
}
%/
    }
  }
/%
for(const QString& ns : _namespaces)
{
  %/
}/%
}%/
