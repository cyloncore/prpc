#ifndef /%= guard %/
#define /%= guard %/

#include <QDateTime>
#include <QList>
#include <QString>
#include <QVariantList>

class QVariant;

#include <pRpc/AbstractService.h>
#include <pRpc/Answer.h>
#include <pRpc/Error.h>
#include <pRpc/Types.h>

namespace pRpc
{
  class Connection;
  class Peer;
  class PeerInfo;
}

/%
for(const QString& ns : _namespaces)
{
  %/
namespace /%= ns %/
{/%
}%/
  template<typename _T_>
  class Answer
  {
  public:
    Answer() {}
    Answer(const ::pRpc::Answer& _answer) : m_answer(_answer) {}
    ~Answer() {}
    bool isAvailable() const { return m_answer.isAvailable(); }
    bool waitForAvailable(int _time = -1) const { return m_answer.waitForAvailable(_time); }
    bool isSuccessful() const { return m_answer.isSuccessful(); }
    void executeOnAvailable(const std::function<void(bool, const _T_&)>& _function);
    _T_ result() const;
    ::pRpc::Error error() const { return m_answer.error(); }
  private:
    ::pRpc::Answer m_answer;
  };/%
  for(const Type* type : _typesManager->types())
  {
#include "Type_h.h"
  }
for(const QString& ns : _namespaces)
{
  %/
}/%
}

foreach(ServiceDefinition* definition, _definitions)
{
  for(const QString& ns : definition->namespaces())
  {
  %/
  namespace /%= ns %/
  {/%
  }
  %/
    namespace /%= definition->name() %/
    {
      /%
      for(const Type* type : definition->typesManager()->types())
      {
#include "Type_h.h"
      }
      %/
      class Caller
      {
      public:
        Caller(::pRpc::Peer* _peer, ::pRpc::Connection _connection);
        ~Caller();
        /%
        foreach(const ServiceDefinition::Call& call, definition->calls())
        {
          %/
        Answer</%= call.returnType->cppName() %/> /%= call.name %/(/%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              if(i != 0)
              {
              %/, /%
              }
              %//%= call.arguments[i].type->cppArgumentName() %/ /%= call.arguments[i].name %//%
            }
            %/);/%
        }
        %/
      private:
        struct Private;
        Private* const d;
      };
      class Service : public ::pRpc::AbstractService
      {
      public:
        Service();
        virtual ~Service();
        static QString name() { return "/%= definition->name() %/"; }
        virtual QVariant handleRequest(::pRpc::Connection _connection, const QString& _method, const QVariantList& _arguments);
      protected:
        /%
        foreach(const ServiceDefinition::Call& call, definition->calls())
        {
          %/
        virtual /%= call.returnType->cppName() %/ /%= call.name %/(::pRpc::Connection _connection/%
            for(int i = 0; i < call.arguments.size(); ++i)
            {
              %/, /%= call.arguments[i].type->cppArgumentName() %/ /%= call.arguments[i].name %//%
            }
            %/) = 0;/%
        }
        %/
      };
    }
  /%
  for(const QString& ns : definition->namespaces())
  {
    Q_UNUSED(ns);
  %/
  }/%
  }
}
%/

#endif
