#include "Type.h"

#include <QList>
#include <QString>

struct Type::Private
{
  Private() : listOf(0) {}
  TType type;
  QString name, namespace_, cppname, cppArgumentName;
  QList<Field> fields;
  mutable const Type* listOf;
};

Type::Type(const QString& _name, const QString& _namespace) : d(new Private)
{
  d->type     = STRUCT;
  d->name     = _name;
  d->namespace_ = _namespace;
  d->cppname  = _name;
  d->cppArgumentName = QString("const %1&").arg(_name);
}

Type::Type(const QString& _name, const QString& _cppname, const QString& _cppArgumentName) : d(new Private)
{
  d->type       = BUILT_IN;
  d->name       = _name;
  d->cppname    = _cppname;
  d->cppArgumentName = _cppArgumentName;
}

Type::Type(const Type* _nested) : d(new Private)
{
  d->type     = LIST;
  d->name     = "list of " + _nested->name();
  d->cppname  = "QList<" + _nested->fullCppName() + ">";
  d->cppArgumentName = "const " + d->cppname + "&";
}

Type::~Type()
{
  delete d->listOf;
  delete d;
}

QString Type::name() const
{
  return d->name;
}

QString Type::namespace_() const
{
  return d->namespace_;
}

QString Type::fullCppName() const
{
  return d->namespace_.isEmpty() ? d->cppname : d->namespace_ + "::" + d->cppname;
}

QString Type::cppName() const
{
  return d->cppname;
}

QString Type::cppArgumentName() const
{
  return d->cppArgumentName;
}

Type::TType Type::type() const
{
  return d->type;
}

void Type::addField(const Type* _type, const QString& _name)
{
  Q_ASSERT(d->type == STRUCT);
  Field f;
  f.name = _name;
  f.type = _type;
  d->fields.append(f);
}

QList< Type::Field > Type::fields() const
{
  return d->fields;
}

const Type* Type::listOf() const
{
  if(d->listOf == 0)
  {
    d->listOf = new Type(this);
  }
  return d->listOf;
}
