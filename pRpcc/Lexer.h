/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class QChar;
class QIODevice;
class QString;

class Token;
class Lexer
{
  public:
    Lexer(QIODevice* sstream);
    ~Lexer();
  public:
    Token nextToken();
    QString readUntil(QChar endChar);
    QString readOneArgument();
  protected:
    /**
     * @return the next char and increment the column counter.
     */
    int getNextChar();
    /**
     * @return the next char that is not a separator (space, tab, return...)
     */
    int getNextNonSeparatorChar();
    /**
     * Cancel the reading of the previous char.
     */
    void unget();
    bool eof() const;
    int line() const;
    int column() const;
    /**
     * Call this function to ignore C++ style comments.
     * @return true if there was a comment, and that _token got fill with
     *         the token after the comment, false other wise (in which case
     *         _token content is undefined)
     */
    bool ignoreComment(Token& _token, int _lastChar  );
    /**
     * Get an identifier (or keyword) in the current flow of character.
     */
    QString getIdentifier(int lastChar);
  private:
    struct Private;
    Private* const d;
};
