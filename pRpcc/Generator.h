#include <QList>

class QString;
class ServiceDefinition;
class TypesManager;

class Generator
{
public:
  Generator();
  ~Generator();
  void generate(const QStringList& _namespaces, const TypesManager* _typesManager, const QList<ServiceDefinition*> _definitions, const QString& _output, bool _has_qml);
};
