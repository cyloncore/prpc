/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ServiceDefinition.h"
#include "TypesManager.h"

struct ServiceDefinition::Private
{
  QStringList nameSpace;
  QString name;
  TypesManager typesManager;
  QList<Call> calls;
};

ServiceDefinition::ServiceDefinition(const QStringList& _namespace, const QString& _name) : d(new Private)
{
  d->name       = _name;
  d->nameSpace  = _namespace;
}

ServiceDefinition::~ServiceDefinition()
{
  delete d;
}

QStringList ServiceDefinition::namespaces() const
{
  return d->nameSpace;
}

QString ServiceDefinition::name() const
{
  return d->name;
}

TypesManager* ServiceDefinition::typesManager()
{
  return &d->typesManager;
}

const TypesManager* ServiceDefinition::typesManager() const
{
  return &d->typesManager;
}

void ServiceDefinition::addCall(const Type* _returnType, const QString& _name, const QList< ServiceDefinition::Call::Argument >& _arguments)
{
  Call c;
  c.returnType = _returnType;
  c.name       = _name;
  c.arguments  = _arguments;
  d->calls.append(c);
}

QList< ServiceDefinition::Call > ServiceDefinition::calls() const
{
  return d->calls;
}

