#include "Generator.h"

#include <QDir>
#include <QFile>
#include <QTextStream>

#include "ServiceDefinition.h"
#include "Type.h"
#include "TypesManager.h"

Generator::Generator()
{

}

Generator::~Generator()
{

}

void Generator::generate(const QStringList& _namespaces, const TypesManager* _typesManager, const QList< ServiceDefinition* > _definitions, const QString& _output, bool _has_qml)
{
  QDir().mkpath(QFileInfo(_output).absolutePath());
  
  if(_has_qml)
  {
    {
      QFile qml_header_file(_output + "Qml.h");
      qml_header_file.open(QIODevice::WriteOnly);
      QTextStream stream(&qml_header_file);
#include "ServiceQml_h.h"
    }
    {
      QFile qml_private_header_file(_output + "Qml_p.h");
      qml_private_header_file.open(QIODevice::WriteOnly);
      QTextStream stream(&qml_private_header_file);
#include "ServiceQml_p.h"
    }
    {
      QFile qml_source_file(_output + "Qml.cpp");
      qml_source_file.open(QIODevice::WriteOnly);
      QTextStream stream(&qml_source_file);
#include "ServiceQml_cpp.h"
    }
  }
  
  {
    QFile header_file(_output + ".h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
    QString guard = ("_" + _namespaces.join("_") + "_H_").toUpper();    
    
#include "Service_h.h"
  }
  {
    QFile body_file(_output + ".cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "Service_cpp.h"
  }
}
