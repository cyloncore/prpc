/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Token.h"

#include <QDebug>

const char* Token::typeToString(Token::Type _type)
{
  switch(_type)
  {
    case UNFINISHED_STRING:
      return "unfinished string";
    case UNFINISHED_COMMENT:
      return "unfinished comment";
    case END_OF_FILE:
      return "end of file";
    case UNKNOWN:
      return "unknown";
    case SEMI:
      return ";";
    case STARTBRACE:
      return "{";
    case ENDBRACE:
      return "}";
    case STARTBRACKET:
      return "(";
    case ENDBRACKET:
      return ")";
    case COMMA:
      return ",";
    case COLONCOLON:
      return "::";
    case IDENTIFIER:
      return "identifier";
    case STRUCT:
      return "struct";
    case SERVICE:
      return "service";
    case NAMESPACE:
      return "namespace";
    case LIST:
      return "list";
    case OF:
      return "of";
  }
  qFatal("add the token");
}

Token::Token() : type(UNKNOWN)
{

}

Token::Token(Type _type, int _line, int _column) : type(_type), line(_line), column(_column)
{
}

Token::Token(Type _type, const QString& _string, int _line, int _column) : type(_type), line(_line), column(_column), string(_string)
{
  Q_ASSERT( _type == IDENTIFIER );
}
