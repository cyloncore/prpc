/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Token.h"

class Type;
class QString;

struct Error;
class Lexer;
class ServiceDefinition;
class TypesManager;

class Parser
{
public:
  Parser(Lexer* _lexer, TypesManager* _manager, const QString& _filename);
  ~Parser();
  QList<ServiceDefinition*> parse();
  QList<Error> errors() const;
  QStringList namespaces() const;
private:
  void parseService();
  const Type* parseStruct(const QString& _namespace);
private:
  const Type* type(const QString& _name);
  const Type* parseType();
private:
  void getNextToken();
private:
  void reportError(const Token& _token, const QString& _errorMsg);
  void reportUnexpected(const Token& _token);
  void reportUnknownIndentifier(const Token& _token);
  bool isOfType(const Token& _token, Token::Type _type);
  bool isOfType(Token::Type _type);
  void reachNext(Token::Type _type);
private:
  struct Private;
  Private* const d;
};
