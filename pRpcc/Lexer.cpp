/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Lexer.h"

#include <QDebug>
#include <QIODevice>

#include "Token.h"

struct Lexer::Private {
  QIODevice* stream;
  int lastChar;
  int col;
  int line;
  int followingnewline;
};

Lexer::Lexer(QIODevice* sstream) : d(new Private)
{
  d->col = 1;
  d->line = 1;
  d->followingnewline = 1;
  d->stream = sstream;
}

Lexer::~Lexer()
{
  delete d;
}

#define IDENTIFIER_IS_KEYWORD( tokenname, tokenid) \
  if( identifierStr == tokenname) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN( tokenchar, tokenid ) \
  if( lastChar == tokenchar ) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN_OR_TOKEN( tokenchar, tokendecidechar, tokenid_1, tokenid_2 ) \
  if( lastChar == tokenchar  ) \
  { \
    if( getNextChar() == tokendecidechar ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN( tokenchar_1, tokenchar_2, tokenchar_3, tokenid_1, tokenid_2, tokenid_3 ) \
  if( lastChar == tokenchar_1 ) \
  { \
    int nextChar = getNextChar(); \
    if( nextChar == tokenchar_2 ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else if( nextChar  == tokenchar_3 ) \
    { \
      return Token(Token::tokenid_3, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

Token Lexer::nextToken()
{
  int lastChar = getNextNonSeparatorChar();
  int initial_line = line() - 1;
  int initial_col = column() - 1;
  if( eof() ) return Token(Token::END_OF_FILE, line(), initial_col);
  QString identifierStr;
  // Test for comment
  Token commentToken;
  if( ignoreComment( commentToken, lastChar ) )
  {
    return commentToken;
  }
  // if it is alpha, it's an identifier or a keyword
  if(isalpha(lastChar) or lastChar == '_')
  {
    identifierStr = getIdentifier(lastChar);
    
    IDENTIFIER_IS_KEYWORD( "struct", STRUCT );
    IDENTIFIER_IS_KEYWORD( "service", SERVICE );
    IDENTIFIER_IS_KEYWORD( "namespace", NAMESPACE );
    IDENTIFIER_IS_KEYWORD( "list", LIST );
    IDENTIFIER_IS_KEYWORD( "of", OF );
    return Token(Token::IDENTIFIER, identifierStr, line(), initial_col);
  } else {
    CHAR_IS_TOKEN(';', SEMI );
    CHAR_IS_TOKEN( '{', STARTBRACE );
    CHAR_IS_TOKEN( '}', ENDBRACE );
    CHAR_IS_TOKEN( '(', STARTBRACKET );
    CHAR_IS_TOKEN( ')', ENDBRACKET );
    CHAR_IS_TOKEN( ',', COMMA );
    CHAR_IS_TOKEN_OR_TOKEN( ':', ':', UNKNOWN, COLONCOLON);
  }
  if( lastChar > 128 ) return nextToken();
  identifierStr = lastChar;
  qDebug() << "Unknown token : " << lastChar << " '" << identifierStr << "' at " << initial_line << "," << initial_col;
  Q_ASSERT( not isspace(lastChar));
  return Token(Token::UNKNOWN, initial_line, initial_col);
}

int Lexer::getNextNonSeparatorChar()
{
  int lastChar = ' ';
  while( not eof() and isspace(lastChar = getNextChar() )  )
  { // Ignore space
  }
  return lastChar;
}

int Lexer::getNextChar()
{
  char nc;
  if(d->stream->getChar(&nc))
  {
    d->lastChar = nc;
    if( nc == '\n' )
    {
      ++d->line;
      ++d->followingnewline;
      d->col = 1;
    } else {
      ++d->col;
      d->followingnewline = 0;
    }
  } else {
    return 0;
  }
  return nc;
}

void Lexer::unget()
{
  --d->col;
  d->stream->ungetChar(d->lastChar);
  if( d->followingnewline > 0 )
  {
    --d->followingnewline;
    --d->line;
  }
}

bool Lexer::eof() const
{
  return d->stream->atEnd();
}

int Lexer::line() const
{
  return d->line;
}
int Lexer::column() const
{
  return d->col;
}

bool Lexer::ignoreComment(Token& _token, int _lastChar )
{
  if( _lastChar == '/' )
  {
    int initial_line = line();
    int initial_col = column();
    int nextChar = getNextChar();
    if( nextChar == '/' )
    { // Mono line comment
      while( not eof() and getNextChar() != '\n' )
      {
      }
      _token = nextToken();
      return true;
    } else if( nextChar == '*' )
    { // Multi line comment
      while( not eof() )
      {
        int nextChar = getNextChar();
        if( nextChar == '*' )
        {
          if( getNextChar() == '/' )
          {
            _token = nextToken();
            return true;
          } else {
            unget();
          }
        }
      }
      _token = Token(Token::UNFINISHED_COMMENT, initial_line, initial_col);
      return true;
    } else {
      unget();
    }
  }
  return false;
}

QString Lexer::getIdentifier(int lastChar)
{
  QString identifierStr;
  if( lastChar != 0) {
    identifierStr = lastChar;
  }
  while (not eof() )
  {
    lastChar = getNextChar();
    if( isalnum(lastChar) or lastChar == '_')
    {
      identifierStr += lastChar;
    } else {
      unget();
      break;
    }
  }
  return identifierStr;
}

QString Lexer::readUntil(QChar endChar)
{
  QString str;
  while(not eof())
  {
    int nextChar = getNextChar();
    if(nextChar == endChar)
    {
      unget();
      return str;
    }
    str += QChar(nextChar);
  }
  return str;
}

QString Lexer::readOneArgument()
{
  QString str;
  bool in_string = false;
  bool skip = false;
  while(not eof())
  {
    int nextChar = getNextChar();
    if(nextChar == '\"')
    {
      in_string = !in_string;
    } else if(not in_string)
    {
      if(nextChar == '\\')
      {
        skip = true;
      } else if(skip)
      {
        skip = false;
      } else if(nextChar == ',' or nextChar == ')')
      {
        unget();
        return str;
      }
    }
    if(not skip)
    {
      str += QChar(nextChar);
    }
  }
  return str;
  
}

