/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QString>
#include <QList>

class TypesManager;
class Type;
class ServiceDefinition
{
public:
  struct Call
  {
    const Type* returnType;
    QString name;
    struct Argument
    {
      const Type* type;
      QString name;
    };
    QList<Argument> arguments;
  };
public:
  ServiceDefinition(const QStringList& _namespace, const QString& _name);
  ~ServiceDefinition();
public:
  QStringList namespaces() const;
  QString name() const;
  TypesManager* typesManager();
  const TypesManager* typesManager() const;
  void addCall(const Type* _returnType, const QString& _name, const QList<Call::Argument>& _arguments);
  QList<Call> calls() const;
private:
  struct Private;
  Private* const d;
};
