/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Parser.h"

#include <QDebug>
#include <QList>
#include <QPair>

#include "Lexer.h"
#include "Error.h"
#include "ServiceDefinition.h"
#include "TypesManager.h"
#include "Type.h"

struct Parser::Private
{
  Lexer* lexer;
  TypesManager* manager;
  Token tok;
  QList<Error> errors;
  QString fileName;
  ServiceDefinition* currentDefinition;
  QList<ServiceDefinition*> definitions;
  QStringList namespaces;
};

Parser::Parser(Lexer* _lexer, TypesManager* _manager, const QString& _filename) : d(new Private)
{
  d->lexer              = _lexer;
  d->fileName           = _filename;
  d->manager            = _manager;
  d->currentDefinition  = 0;
}

Parser::~Parser()
{
  delete d;
}

QStringList Parser::namespaces() const
{
  return d->namespaces;
}

QList< ServiceDefinition* > Parser::parse()
{
  // Start Parsing
  getNextToken();
  while(d->tok.type != Token::END_OF_FILE)
  {
    switch(d->tok.type)
    {
      case Token::NAMESPACE:
      {
        getNextToken();
        while(d->tok.type != Token::END_OF_FILE)
        {
          if(isOfType(Token::IDENTIFIER))
          {
            d->namespaces.append(d->tok.string);
            getNextToken();
            if(d->tok.type != Token::COLONCOLON)
            {
              break;
            }
            getNextToken(); // eats '::'
          }
        }
        break;
      }
      case Token::STRUCT:
      {
        const Type* t = parseStruct(QString());
        if(t)
        {
          if(not d->manager->addType(t))
          {
            reportError(d->tok, QString("%1 is already defined").arg(t->name()));
            delete t;
            t = 0;
          }
        }
      }
        break;
      case Token::SERVICE:
      {
        parseService();
        break;
      }
      default:
      {
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);
        break;
      }
    }
    isOfType(Token::SEMI);
    getNextToken();
  }
  // Check if errors have occured
  if(not d->errors.empty())
  {
    qDeleteAll(d->definitions);
    d->definitions.clear();
  }
  return d->definitions;
}

void Parser::parseService()
{
  isOfType(Token::SERVICE);
  getNextToken();
  if(isOfType(Token::IDENTIFIER))
  {
    d->currentDefinition = new ServiceDefinition(d->namespaces, d->tok.string);
    getNextToken();
    if(isOfType(Token::STARTBRACE))
    {
      getNextToken();
      while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
      {
        switch(d->tok.type)
        {
          case Token::LIST:
          case Token::IDENTIFIER:
          {
            const Type* returnType = parseType();
            if(isOfType(Token::IDENTIFIER))
            {
              QString funcname = d->tok.string;
              getNextToken();
              if(isOfType(Token::STARTBRACKET))
              {
                getNextToken();
                QList< ServiceDefinition::Call::Argument > arguments;
                if(d->tok.type != Token::ENDBRACKET)
                {
                  while(true)
                  {
                    const Type* argumentType = parseType();
                    if(argumentType)
                    {
                      ServiceDefinition::Call::Argument argument;
                      argument.type = argumentType;
                      if(isOfType(Token::IDENTIFIER))
                      {
                        argument.name = d->tok.string;
                        arguments.append(argument);
                      }
                    }
                    getNextToken();
                    if(d->tok.type == Token::COMMA)
                    {
                      getNextToken();
                    } else {
                      break;
                    }
                  }
                }
                d->currentDefinition->addCall(returnType, funcname, arguments);
                isOfType(Token::ENDBRACKET);
                getNextToken();
                if(not isOfType(Token::SEMI))
                {
                  reachNext(Token::SEMI);
                }
              }
            }
          }
            break;
          case Token::STRUCT:
          {
            const Type* t = parseStruct(d->currentDefinition->name());
            if(t)
            {
              if(not d->currentDefinition->typesManager()->addType(t))
              {
                reportError(d->tok, QString("%1 is already defined").arg(t->name()));
                delete t;
                t = 0;
              }
            }
          }
            break;
          default:
            reportUnexpected(d->tok);
            reachNext(Token::SEMI);
            break;
        }
        getNextToken();
      }
    
      isOfType(Token::ENDBRACE);
      getNextToken();
      d->definitions.append(d->currentDefinition);
      return;
    }
  }
  delete d->currentDefinition;
  d->currentDefinition = 0;
  reachNext(Token::SEMI);
}

const Type* Parser::parseStruct(const QString& _namespace)
{
  isOfType(Token::STRUCT);
  getNextToken();
  if(isOfType(Token::IDENTIFIER))
  {
    Type* t = new Type(d->tok.string, _namespace);
    getNextToken();
    if(isOfType(Token::STARTBRACE))
    {
      getNextToken();
      while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
      {
        const Type* tf = parseType();
        if(tf)
        {
          if(isOfType(Token::IDENTIFIER))
          {
            t->addField(tf, d->tok.string);
            getNextToken();
            isOfType(Token::SEMI);
          }
          getNextToken();
        }
      }
      isOfType(Token::ENDBRACE);
      getNextToken();
      return t;
    } else {
      reachNext(Token::SEMI);
      delete t;
      return 0;
    }
  }
  return 0;
}

const Type* Parser::type(const QString& _name)
{
  const Type* t = d->manager->type(_name);
  if(t)
  {
    return t;
  }
  if(d->currentDefinition)
  {
    t = d->currentDefinition->typesManager()->type(_name);
    if(t)
    {
      return t;
    }
  }
  reportError(d->tok, QString("Unknown type %1").arg(_name));
  return 0;
}

const Type* Parser::parseType()
{
  bool is_list = false;
  if(d->tok.type == Token::LIST)
  {
    is_list = true;
    getNextToken();
    if(isOfType(Token::OF))
    {
      getNextToken();
    }
  }
  if(isOfType(Token::IDENTIFIER))
  {
    const Type* t = type(d->tok.string);
    getNextToken();
    if(is_list and t)
    {
      return t->listOf();
    } else {
      return t;
    }
  }
  getNextToken();
  return 0;
}

void Parser::getNextToken()
{
  d->tok = d->lexer->nextToken();
}

bool Parser::isOfType(const Token& _token, Token::Type _type)
{
  if(_token.type == _type) return true;
  reportError(_token, QString("Expected token %1 got %2").arg(Token::typeToString(_type)).arg(Token::typeToString(_token.type)));
  return false;
}

bool Parser::isOfType(Token::Type _type)
{
  return isOfType(d->tok, _type);
}

void Parser::reachNext(Token::Type _type)
{
  while(d->tok.type != Token::END_OF_FILE and d->tok.type != _type)
  {
    getNextToken();
  }
}

QList< Error > Parser::errors() const
{
  return d->errors;
}

void Parser::reportError(const Token& _token, const QString& _errorMsg)
{
  Error err;
  err.line      = _token.line;
  err.column    = _token.column;
  err.filename  = d->fileName;
  err.message   = _errorMsg;
  d->errors.append(err);
}

void Parser::reportUnexpected(const Token& _token)
{
  reportError(_token, QString("Unexpected token %1").arg(Token::typeToString(_token.type)));
}

void Parser::reportUnknownIndentifier(const Token& _token)
{
  Q_ASSERT(_token.type == Token::IDENTIFIER);
  reportError(_token, QString("Unknown identifier %1").arg(_token.string));
}
