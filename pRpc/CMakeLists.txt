include_directories(${CMAKE_CURRENT_BINARY_DIR})

set(PRPC_SRCS
    Peer.cpp MessageHeader.cpp AbstractTransport.cpp TcpTransport.cpp AbstractService.cpp
    AbstractConnectionModel.cpp
    Answer.cpp
    AutoConnection.cpp
    ClientPasswordAuthentication.cpp
    Connection_p.cpp
    Connection.cpp
    Error.cpp
    Errors.cpp
    PeerInfo.cpp
    TcpConnectionModel.cpp
    BinaryHeader.cpp NoAuthentication.cpp AbstractAuthentication.cpp ConnectionsThread.cpp


    )

set(PRPC_HEADERS
    Peer.h AbstractTransport.h TcpTransport.h AbstractService.h PeerInfo.h
    Answer.h Error.h AbstractAuthentication.h NoAuthentication.h
    AbstractConnectionModel.h
    AutoConnection.h
    Connection.h
    ClientPasswordAuthentication.h
    MessageHeader.h
    Types.h)

if(Qt5Core_VERSION VERSION_GREATER "5.10.0")
  set(PRPC_SRCS ${PRPC_SRCS} ServerPasswordAuthentication.cpp)
  set(PRPC_HEADERS ${PRPC_HEADERS} ServerPasswordAuthentication.h)
endif()

if(${IOS})
  set(MODE STATIC)
else()
  set(MODE SHARED)
endif()

add_library(pRpc ${MODE} ${PRPC_SRCS})
target_link_libraries(pRpc Qt5::Network Qt5::Concurrent)

install(TARGETS pRpc EXPORT pRpcTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
install(FILES ${PRPC_HEADERS}
        DESTINATION ${INSTALL_INCLUDE_DIR}/pRpc )
