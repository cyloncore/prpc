/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace pRpc
{
  enum ErrorCode
  {
    EC_USER                     = 0,
    EC_INVALID                  = -1,
    EC_UNKNOWN                  = -2,
    EC_INVALID_JSON             = -3,
    EC_UNKNOWN_SERVICE          = -100,
    EC_UNKNOWN_METHOD           = -101,
    EC_INVALID_ARGUMENTS_COUNT  = -102
  };
}
