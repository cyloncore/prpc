/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "PeerInfo.h"

#include <QString>

using namespace pRpc;

struct PeerInfo::Private : public QSharedData
{
  QString name, id, connection;
};

PeerInfo::PeerInfo() : d(new Private)
{
  
}

PeerInfo::PeerInfo(const QString& _name, const QString& _id, const QString& _connection) : d(new Private)
{
  d->name       = _name;
  d->id         = _id;
  d->connection = _connection;
}

PeerInfo::PeerInfo(const PeerInfo& _rhs) : d(_rhs.d)
{

}

PeerInfo& PeerInfo::operator=(const PeerInfo& _rhs)
{
  d = _rhs.d;
  return *this;
}

PeerInfo::~PeerInfo()
{

}

QString PeerInfo::address() const
{
  return d->connection;
}

QString PeerInfo::name() const
{
  return d->name;
}

QString PeerInfo::id() const
{
  return d->id;
}
