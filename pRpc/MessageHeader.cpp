/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "MessageHeader.h"

#include <QDataStream>

using namespace pRpc;

struct MessageHeader::Private : public QSharedData
{
  quint64 size;
  quint16 type;
  bool valid;
};

MessageHeader::MessageHeader() : d(new Private)
{
  d->valid = false;
}

MessageHeader::MessageHeader(quint64 _size, quint16 _type) : d(new Private)
{
  d->size   = _size;
  d->type   = _type;
  d->valid  = true;
}

MessageHeader::MessageHeader(const MessageHeader& _rhs) : d(_rhs.d)
{

}

MessageHeader& MessageHeader::operator=(const MessageHeader& _rhs)
{
  d = _rhs.d;
  return *this;
}

MessageHeader::~MessageHeader()
{

}

quint64 MessageHeader::size() const
{
  return d->size;
}

quint16 MessageHeader::type() const
{
  return d->type;
}

bool MessageHeader::isValid() const
{
  return d->valid;
}

QDataStream& pRpc::operator<<(QDataStream& stream, const MessageHeader& _header)
{
  stream << _header.size() << _header.type();
  return stream;
}

QDataStream& pRpc::operator>>(QDataStream& stream, MessageHeader& _header)
{
  _header.d.detach();
  stream >> _header.d->size>> _header.d->type;
  _header.d->valid = true;
  return stream;
}
