/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "NoAuthentication.h"

using namespace pRpc;

NoAuthentication::NoAuthentication()
{

}

NoAuthentication::~NoAuthentication()
{

}

QVariantMap NoAuthentication::handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload)
{
  return QVariantMap();
}

bool NoAuthentication::authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload)
{
  return true;
}

QVariantMap NoAuthentication::requestAuthentication(const PeerInfo& _info)
{
  return QVariantMap();
}
