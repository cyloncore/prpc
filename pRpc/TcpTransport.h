/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractTransport.h"

#include <QHostAddress>

namespace pRpc
{
  class TcpTransportServer;
  class TcpTransport : public AbstractTransport
  {
    friend class TcpTransportServer;
  public:
    TcpTransport();
    virtual ~TcpTransport();
    /**
     * Start listening
     */
    bool listen(const QHostAddress& address = QHostAddress::Any, quint16 port = 0);

    AbstractConnectionModel* connectTo(const QString& _address) override;
  private:
    AbstractConnectionModel* connectTo(const QHostAddress& address, quint16 port);
  private:
    struct Private;
    Private* const d;
  };
}

//     /**
//      * Advertise the service through Bonjour/Avahi
//      */
//     void advertise(const QString& _name, const QString& _serviceType, const QString& _domain);
