/*
 *  Copyright (c) 2013,2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ConnectionsThread_p.h"

#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>

#include "AbstractAuthentication.h"
#include "AbstractConnectionModel.h"
#include "Connection_p.h"
#include "Peer_p.h"

using namespace pRpc;

struct ConnectionsThread::Private
{
  Peer* peer;
  QThreadPool* threadPool;
};

ConnectionsThread::ConnectionsThread(Peer* _peer, QThreadPool* _threadPool) : d(new Private)
{
  d->peer = _peer;
  d->threadPool = _threadPool;
}

ConnectionsThread::~ConnectionsThread()
{
}

int ConnectionsThread::activeConnections()
{
  return 0;
}

void ConnectionsThread::startConnection(Connection _connection)
{
  _connection.d->threadPool = d->threadPool;
  _connection.d->moveToThread(this);
  _connection.d->model->moveToThread(this);
  _connection.d->peer = d->peer;
  QMetaObject::invokeMethod(_connection.d, "doStartConnection", Qt::QueuedConnection);
}
