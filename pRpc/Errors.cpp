/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Errors.h"

#include <QString>

#include "ErrorCode.h"

using namespace pRpc;

Error Errors::InvalidJson           = Error(EC_INVALID_JSON,            "Invalid Json");
Error Errors::UnknownService        = Error(EC_UNKNOWN_SERVICE,         "Unknown service");
Error Errors::UnknownMethod         = Error(EC_UNKNOWN_METHOD,          "Unknown method");
Error Errors::InvalidArgumentsCount = Error(EC_INVALID_ARGUMENTS_COUNT, "Invalid arguments count");
