/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class QString;

#include <QSharedDataPointer>

namespace pRpc
{
  class Error
  {
  public:
    Error();
    Error(int _code, const QString& _message);
    Error(const Error& _rhs);
    Error& operator=(const Error& _rhs);
    ~Error();
  public:
    int code() const;
    QString message() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
}

#include <QMetaType>
Q_DECLARE_METATYPE(pRpc::Error)
