/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "TcpConnectionModel_p.h"

#include <QHostAddress>

using namespace pRpc;

TcpConnectionModel::TcpConnectionModel(const QHostAddress& address, quint16 port) : m_socketDescriptor(0), m_address(address), m_port(port)
{
}

TcpConnectionModel::TcpConnectionModel(qintptr socketDescriptor) : m_socketDescriptor(socketDescriptor)
{
}

TcpConnectionModel::~TcpConnectionModel()
{
}

bool TcpConnectionModel::close()
{
  if(device())
  {
    device()->close();
  }
  return true;
}

void TcpConnectionModel::initialise()
{
  Q_ASSERT(not device());

  QTcpSocket* socket = new QTcpSocket(this);
  if(m_socketDescriptor)
  {
    socket->setSocketDescriptor(m_socketDescriptor);
    m_socketDescriptor = 0;
  } else {
    socket->connectToHost(m_address, m_port);
  }
  
  connect(socket, SIGNAL(disconnected()), SIGNAL(disconnected()));
  
  if(!socket->waitForConnected(5000))
  {
    setDevice(socket);
    error(socket->error());
    setDevice(nullptr);
    
    socket->deleteLater();
    socket = nullptr;
    
  } else {
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(error(QAbstractSocket::SocketError)), Qt::DirectConnection);
  }
  setDevice(socket);
}

void TcpConnectionModel::error(QAbstractSocket::SocketError error)
{
  QTcpSocket* socket = qobject_cast<QTcpSocket*>(device());
  qWarning() << "TcpConnectionModel::error " << error << " connected to " << socket->peerAddress() << " " << socket->errorString();
  emit(connectionError(socket->errorString()));
  emit(disconnected());
}


QString TcpConnectionModel::address() const
{
  const QTcpSocket* socket = qobject_cast<const QTcpSocket*>(device());
  return socket->peerAddress().toString() + ":" + QString::number(socket->peerPort());
}
