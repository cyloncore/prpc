/*
 *  Copyright (c) 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PRPC_CONNECTION_H_
#define _PRPC_CONNECTION_H_

class QIODevice;
class QObject;

#include <functional>

namespace pRpc
{
  class AbstractConnectionModel;
  class ConnectionsThread;
  class Peer;
  class PeerInfo;
  class Connection
  {
    friend class AbstractConnectionModel;
    friend class AbstractService;
    friend class ConnectionsThread;
    friend class Peer;
  public:
    enum class Status {
      Unknown,        //< status of the connection is not known
      Connected,      //< the connection between the two peers is established
      Connecting,     //< the connection is being established
      Disconnected   //< the connection is disconnected
  };
    Connection(AbstractConnectionModel* _model);
  public:
    Connection();
    Connection(const Connection& _rhs);
    Connection& operator=(const Connection& _rhs);
    ~Connection();
    bool isValid() const;
    void clear();
    void connect(const char *signal, const QObject *receiver, const char *member);
    void onEstablishedConnection(const std::function<void()>& _functor);
    void onDisconnection(const std::function<void()>& _functor);
    bool operator!=(const Connection& _rhs) const;
  public:
    PeerInfo remotePeerInfo() const;
    Peer* peer() const;
    /**
     * Request to close the connection.
     */
    void requestClose();
    Status status() const;
  private:
    struct Private;
    Private* d;
    void deref();
    void set(Private* _d);
  };
}

#endif
