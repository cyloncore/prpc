/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractAuthentication.h"

using namespace pRpc;

struct AbstractAuthentication::Private
{
  
};

AbstractAuthentication::AbstractAuthentication() : d(0)
{

}

AbstractAuthentication::~AbstractAuthentication()
{
  delete d;
}
