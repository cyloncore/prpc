/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class QIODevice;
class QByteArray;
class QString;

namespace pRpc 
{
  class AbstractConnectionModel;
  class MessageHeader;
  class Peer;

  /**
   * @brief This class represents a type of transport to connect to distant host.
   * It handles incoming connection and allow the connection to a remote address.
   */
  class AbstractTransport
  {
    friend class Peer;
  public:
    AbstractTransport();
    virtual ~AbstractTransport();
  protected:
    /**
     * Call this function to handle an incoming connection from a remote peer
     */
    void handleIncomingConnection(AbstractConnectionModel* _connection);
    /**
     * Create a new connection to a peer at the given @p _address
     */
    virtual AbstractConnectionModel* connectTo(const QString& _address) = 0;
  private:
    struct Private;
    Private* const d;
  };
}
