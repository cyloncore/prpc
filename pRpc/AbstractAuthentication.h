/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <QVariant>

namespace pRpc
{
  class PeerInfo;
  class AbstractAuthentication
  {
  public:
    AbstractAuthentication();
    virtual ~AbstractAuthentication();
    /**
     * This function is executed at the begining of the authentication, the returned map is send to the other client
     */
    virtual QVariantMap requestAuthentication(const PeerInfo& _info) = 0;
    /**
     * This function is executed when the client receive the 'requestAuthentication' payload
     */
    virtual QVariantMap handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload) = 0;
    /**
     * This function is used to authenticate the other client with the result of their own call to handleAuthenticationRequest
     */
    virtual bool authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload) = 0;
  private:
    struct Private;
    Private* const d;
  };
}
