/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <qglobal.h>
#include <QSharedData>

namespace pRpc
{
  class MessageHeader
  {
    friend QDataStream &operator>>(QDataStream& stream, pRpc::MessageHeader& _header);
  public:
    enum Type
    {
      T_JSON, T_STREAM, T_DATA
    };
  public:
    enum Fields
    {
      F_SIZE = 0,                         // size 64 bits unsigned
      F_TYPE = sizeof(quint64) + F_SIZE,  // type 16 bits unsigned
      F_END  = sizeof(quint16) + F_TYPE   // end of the header§1
    };
  public:
    MessageHeader();
    /**
     * @param _size of the data ! the size of the header is ignored
     * @param _type the type of content
     */
    MessageHeader(quint64 _size, quint16 _type);
    MessageHeader(const MessageHeader& _rhs);
    MessageHeader& operator=(const MessageHeader& _rhs);
    ~MessageHeader();
    quint64 size() const;
    quint16 type() const;
    bool isValid() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  QDataStream &operator<<(QDataStream& stream, const pRpc::MessageHeader& _header);
  QDataStream &operator>>(QDataStream& stream, pRpc::MessageHeader& _header);
}
