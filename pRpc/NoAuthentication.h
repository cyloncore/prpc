/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "AbstractAuthentication.h"

namespace pRpc
{
  class NoAuthentication : public AbstractAuthentication
  {
  public:
    NoAuthentication();
    virtual ~NoAuthentication();
    virtual bool authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload);
    virtual QVariantMap handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload);
    virtual QVariantMap requestAuthentication(const PeerInfo& _info);
  };
}
