/*
 *  Copyright (c) 2013,2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractConnectionModel.h"

#include <QBuffer>
#include <QDataStream>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "Peer_p.h"

using namespace pRpc;

struct AbstractConnectionModel::Private
{
  QIODevice* device = nullptr;
};

AbstractConnectionModel::AbstractConnectionModel() : d(new Private)
{
}

AbstractConnectionModel::~AbstractConnectionModel()
{
  if(d->device) d->device->deleteLater();
  delete d;
}

QIODevice* AbstractConnectionModel::device()
{
  return d->device;
}

const QIODevice* AbstractConnectionModel::device() const
{
  return d->device;
}

void AbstractConnectionModel::setDevice(QIODevice* _device)
{
  d->device = _device;
}


#include "moc_AbstractConnectionModel.cpp"
