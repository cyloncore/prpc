/*
 *  Copyright (c) 2013,2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Peer.h"

#include <QMutex>
#include <QThreadPool>

namespace pRpc
{
  class ConnectionsThread;
  struct Peer::Private
  {
    QString                           name, id;
    AbstractTransport*                transport;
    Peer*                             self;
    
    QMutex authenticationMutex;
    AbstractAuthentication*           authentication;
    
    QMutex servicesMutex;
    QHash<QString, AbstractService*>  services;
    
    QList<Connection> incomingConnections, outgoingConnections;
    
    QList<ConnectionsThread*> connectionsThreads;
    int maxConnectionsThreads;
    ConnectionsThread* getConnectionsThread();
    
    QThreadPool threadPool;
    
    void handleIncomingConnection(Connection _connection);
  };
}
