/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <qglobal.h>
#include <QSharedData>

namespace pRpc
{
  class BinaryHeader
  {
    friend QDataStream &operator>>(QDataStream& stream, pRpc::BinaryHeader& _header);
  public:
    enum Fields
    {
      F_ID = 0,                           // id 64 bits unsigned
      F_SERVICE_NAME_SIZE = sizeof(quint64) + F_ID,
      F_END  = sizeof(quint64) + F_SERVICE_NAME_SIZE   // end of the header§1
    };
  public:
    BinaryHeader();
    /**
     * @param _size of the data ! the size of the header is ignored
     * @param _type the type of content
     */
    BinaryHeader(quint64 _id, quint64 _serviceNameSize);
    BinaryHeader(const BinaryHeader& _rhs);
    BinaryHeader& operator=(const BinaryHeader& _rhs);
    ~BinaryHeader();
    quint64 id() const;
    quint64 serviceNameSize() const;
    bool isValid() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  QDataStream &operator<<(QDataStream& stream, const pRpc::BinaryHeader& _header);
  QDataStream &operator>>(QDataStream& stream, pRpc::BinaryHeader& _header);
}
