/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QSharedDataPointer>

class QString;

namespace pRpc
{
  class PeerInfo
  {
  public:
    PeerInfo();
    PeerInfo(const QString& _name, const QString& _id, const QString& _connection);
    PeerInfo(const PeerInfo& _rhs);
    PeerInfo& operator=(const PeerInfo& _rhs);
    ~PeerInfo();
    QString name() const;
    QString id() const;
    QString address() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
}
