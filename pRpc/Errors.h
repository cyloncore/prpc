/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Error.h"

namespace pRpc {
  namespace Errors
  {
    extern Error InvalidJson;
    extern Error UnknownService;
    extern Error UnknownMethod;
    extern Error InvalidArgumentsCount;
  }
}
