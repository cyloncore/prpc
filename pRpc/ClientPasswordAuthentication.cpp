/*
 *  Copyright (c) 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ClientPasswordAuthentication.h"

#include <QCryptographicHash>

using namespace pRpc;

struct ClientPasswordAuthentication::Private
{
  QByteArray password;
  int version;
};

ClientPasswordAuthentication::ClientPasswordAuthentication() : d(new Private)
{
}

ClientPasswordAuthentication::~ClientPasswordAuthentication()
{
}

void ClientPasswordAuthentication::setPassword(const QByteArray& _password)
{
  d->password     = _password;
}

bool ClientPasswordAuthentication::authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload)
{
  return true;
}

#define HASH_PASSWORD(_HASH_ALG_)                               \
  else if(hash == # _HASH_ALG_)                                 \
  {                                                             \
    QCryptographicHash qch(QCryptographicHash::_HASH_ALG_);     \
    qch.reset();                                                \
    qch.addData(salt);                                          \
    qch.addData(d->password);                                   \
    passwordHash = qch.result();                                \
  }

QVariantMap ClientPasswordAuthentication::handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload)
{
  QByteArray passwordHash;
  
  QString hash = _payload["hash"].toString();
  QByteArray salt = QByteArray::fromBase64(_payload["salt"].toString().toLatin1());
  if(false) {}
  HASH_PASSWORD(Sha224)
  HASH_PASSWORD(Sha256)
  HASH_PASSWORD(Sha512)
  HASH_PASSWORD(Sha3_224)
  HASH_PASSWORD(Sha3_256)
  HASH_PASSWORD(Sha3_384)
  HASH_PASSWORD(Sha3_512)
  
  QVariantMap map;
  map["password"] = QString::fromLatin1(passwordHash.toBase64());
  map["version"]  = d->version;
  return map;
}

QVariantMap ClientPasswordAuthentication::requestAuthentication(const PeerInfo& _info)
{
  return QVariantMap();
}
