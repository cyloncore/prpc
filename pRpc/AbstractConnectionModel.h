/*
 *  Copyright (c) 2013,2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PRPC_ABSTRACTCONNECTION_H_
#define _PRPC_ABSTRACTCONNECTION_H_

#include <QObject>

class QIODevice;

#include "Connection.h"

namespace pRpc
{
  /**
   * @brief This class represents a connection to a distant peer
   */
  class AbstractConnectionModel : public QObject
  {
    Q_OBJECT
    friend class Connection::Private;
  public:
    AbstractConnectionModel();
    virtual ~AbstractConnectionModel();
  public:
    /**
     * @return the address used to connect to the remote peer
     */
    virtual QString address() const = 0;
  protected:
    QIODevice* device();
    const QIODevice* device() const;
    void setDevice(QIODevice* _device);
    virtual bool close() = 0;
    /**
     * This function is called to initialise the connection, it should call setDevice with a valid device in case of success
     */
    virtual void initialise() = 0;
  signals:
    /**
     * This signal is emited when an error happen on the connection
     */
    void connectionError(const QString& _errorMessage);
    /**
     * This signal is emited when the connection is established.
     */
    void connected();
    /**
     * This signal is emited when the connection is lost
     */
    void disconnected();
  private:
    struct Private;
    Private* const d;
  };
}

#endif
