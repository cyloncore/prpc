/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Answer.h"

#include <QMutex>
#include <QVariant>
#include <QWaitCondition>

namespace pRpc {
  struct Answer::Private
  {
    Private(bool _validRequest) : isAvailable(false), validRequest(_validRequest), result("No answer"), isSuccessfull(false) {}
    
    bool isAvailable;
    bool validRequest;
    QVariant result;
    bool isSuccessfull;
    
    QList<std::function<void(bool, const QVariant&)>> functions;
    
    mutable QMutex waitAvailableMutex;
    mutable QWaitCondition waitAvailableCondition;
    
    void setResult(bool _success, const QVariant& _value)
    {
      QMutexLocker l(&waitAvailableMutex);
      isAvailable   = true;
      isSuccessfull = _success;
      result        = _value;
      for(const std::function<void(bool, const QVariant&)>& func : functions)
      {
        func(isSuccessfull, _value);
      }
      functions.clear();
      waitAvailableCondition.wakeAll();
    }
  };
}
