/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractConnectionModel.h"

#include <QHostAddress>
#include <QTcpSocket>

namespace pRpc
{
  class TcpConnectionModel : public AbstractConnectionModel
  {
    Q_OBJECT
  public:
    TcpConnectionModel(const QHostAddress& address, quint16 port);
    TcpConnectionModel(qintptr socketDescriptor);
    ~TcpConnectionModel();
  protected:
    bool close() override;
    void initialise() override;
    QString address() const override;
  private slots:
    void error(QAbstractSocket::SocketError);
  private:
    qintptr m_socketDescriptor;
    QHostAddress m_address;
    quint16 m_port;
  };
}
