/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractTransport_p.h"

#include "Connection.h"
#include "Peer_p.h"

using namespace pRpc;

AbstractTransport::AbstractTransport() : d(new Private)
{

}

AbstractTransport::~AbstractTransport()
{
  delete d;
}

void AbstractTransport::handleIncomingConnection(AbstractConnectionModel* _connection)
{
  if(d->peer)
  {
    d->peer->d->handleIncomingConnection(_connection);
  }
}
