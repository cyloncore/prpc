/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Error.h"

#include <QString>

#include "ErrorCode.h"

using namespace pRpc;

struct Error::Private : public QSharedData
{
  int     code;
  QString message;
};

Error::Error() : d(new Private)
{
  d->code = EC_INVALID;
}

Error::Error(int _code, const QString& _message) : d(new Private)
{
  d->code     = _code;
  d->message  = _message;
}

Error::Error(const Error& _rhs) : d(_rhs.d)
{
  
}

Error& Error::operator=(const Error& _rhs)
{
  d = _rhs.d;
  return *this;
}

Error::~Error()
{
}

int Error::code() const
{
  return d->code;
}

QString Error::message() const
{
  return d->message;
}
