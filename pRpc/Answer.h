/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QSharedPointer>

#include <functional>

class QVariant;

namespace pRpc
{
  class Connection;
  class Error;
  class Answer
  {
    friend class Connection;
  private:
    struct Private;
  private:
    Answer(QSharedPointer<Private> d);
  public:
    Answer();
    Answer(const Answer& _rhs);
    Answer& operator=(const Answer& _rhs);
    ~Answer();
    bool isAvailable() const;
    /**
     * Wait until the data is available or the time has been reached.
     * @param _time is expressed in miliseconds
     */
    bool waitForAvailable(int _time = -1) const;
    /**
     * Execute when the answer is available.
     * The first argument is true if the result is sucessfull and the second one is the value
     * 
     * The function is called immediately if the result is already available
     */
    void executeOnAvailable(const std::function<void(bool, const QVariant&)>& _function);
    bool isSuccessful() const;
    QVariant result() const;
    Error error() const;
  private:
    QSharedPointer<Private> d;
  };
}
