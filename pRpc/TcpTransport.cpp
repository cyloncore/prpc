/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "TcpTransport.h"

#include <QByteArray>
#include <QHostInfo>
#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>

#include "Connection.h"
#include "TcpConnectionModel_p.h"

using namespace pRpc;

namespace pRpc
{
  class TcpTransportServer : public QTcpServer
  {
  public:
    TcpTransportServer(TcpTransport* _transport, QObject * parent = 0) : QTcpServer(parent), m_transport(_transport)
    {
    }
    void incomingConnection(qintptr socketDescriptor);
  private:
    TcpTransport* m_transport;
  };
}

void TcpTransportServer::incomingConnection(qintptr socketDescriptor)
{
  m_transport->handleIncomingConnection(new TcpConnectionModel(socketDescriptor));
}

struct TcpTransport::Private
{
  Private() : server(0) { }
  TcpTransportServer* server;
};

TcpTransport::TcpTransport() : d(new Private)
{

}

TcpTransport::~TcpTransport()
{
  delete d->server;
  delete d;
}

AbstractConnectionModel* TcpTransport::connectTo(const QHostAddress& address, quint16 port)
{
  return new TcpConnectionModel(address, port);
}

bool TcpTransport::listen(const QHostAddress& address, quint16 port)
{
  d->server = new TcpTransportServer(this);
  return d->server->listen(address, port);
}

AbstractConnectionModel* TcpTransport::connectTo(const QString& _address)
{
  QStringList parts = _address.split(":");
  QString host;
  int port;
  
  if(parts.size() == 1)
  {
    host = _address;
    port = -1;
  } else {
    host = parts[0];
    port = parts[1].toInt();
  }
  
  QList<QHostAddress> addresses = QHostInfo::fromName(host).addresses();

  if(addresses.empty()) return 0;
  
  return connectTo(addresses.first(), port);
}

// void Peer::advertise(const QString& _name, const QString& _serviceType, const QString& _domain)
// {
//   qFatal("Unimplemented");
// }
// 
