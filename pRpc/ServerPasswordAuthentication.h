/*
 *  Copyright (c) 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractAuthentication.h"

#include <QCryptographicHash>

namespace pRpc
{
  class ServerPasswordAuthentication : public AbstractAuthentication
  {
  public:
    ServerPasswordAuthentication(QCryptographicHash::Algorithm _hashAlgorithm);
    virtual ~ServerPasswordAuthentication();
    virtual bool authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload);
    virtual QVariantMap handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload);
    virtual QVariantMap requestAuthentication(const PeerInfo& _info);
  protected:
    virtual QByteArray password(const QString& _name, QCryptographicHash::Algorithm _hashAlgorithm) = 0;
  private:
    struct Private;
    Private* const d;
  };
}


