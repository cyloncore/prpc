/*
 *  Copyright (c) 2013,2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Connection_p.h"

#include <QBuffer>
#include <QDataStream>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHash>
#include <QMutex>
#include <QTimer>

#include <QtConcurrent>

#include "AbstractAuthentication.h"
#include "AbstractConnectionModel.h"
#include "AbstractService_p.h"
#include "BinaryHeader.h"
#include "Errors.h"
#include "ErrorCode.h"
#include "Peer_p.h"

using namespace pRpc;

Connection::Private::Private(AbstractConnectionModel* _model) : peer(0), model(_model), status(Status::Unknown), nextRequestId(0)
{
  connect(_model, SIGNAL(disconnected()), SIGNAL(disconnected()));
  connect(_model, SIGNAL(connected()), SIGNAL(connected()));
  connect(_model, SIGNAL(connectionError(QString)), SLOT(reportError(const QString& )));
}

MessageHeader Connection::Private::readMessageHeader()
{
  MessageHeader mh;
  QIODevice* device = model->device();
  if(device->bytesAvailable() > MessageHeader::F_END)
  {
    QByteArray data = device->read(MessageHeader::F_END);
    QBuffer buff(&data);
    buff.open(QIODevice::ReadOnly);
    QDataStream stream(&buff);
    stream >> mh;
  }
  return mh;
}

void Connection::Private::sendData(MessageHeader::Type _type, const QByteArray& _data)
{
  QIODevice* device = model->device();
  QDataStream stream(device);
  stream << MessageHeader(_data.size(), _type);
  device->write(_data);
}


void Connection::Private::readData()
{
  QIODevice* device = model->device();
  if(not currentHeader.isValid())
  {
    currentHeader = readMessageHeader();
  }
  if(currentHeader.isValid())
  {
    switch(currentHeader.type())
    {
    case MessageHeader::T_DATA:
      {
        if(device->bytesAvailable() < int(currentHeader.size())) return; // Need more data
        qFatal("Unimplemented MessageHeader::T_DATA"); // Actually not sure how to handle those message, should there be a small header to indicate the service ?
        break;
      }
    case MessageHeader::T_JSON:
      {
        if(device->bytesAvailable() < int(currentHeader.size())) return; // Need more data
        handleJson(device->read(currentHeader.size()));
        break;
      }
    case MessageHeader::T_STREAM:
      {
        handleStream(currentHeader.size());
        break;
      }
    default:
      {
        if(device->bytesAvailable() < int(currentHeader.size())) return; // Need more data
        device->read(currentHeader.size()); // skip the message
        // TODO report error
      }
    }
    currentHeader = MessageHeader();
    if(device->bytesAvailable() > 0) readData();
  }
}


Answer Connection::Private::request(const QString& _service, const QString& _method, const QVariantList& _parameters)
{
  int id = ++nextRequestId;
  Private::Request req;
  req.id          = id;
  req.service     = _service;
  req.method      = _method;
  req.arguments   = _parameters;
  QSharedPointer<Answer::Private> ap(new Answer::Private(true));
  { // Add the answer first, to be sure that it is properly set for request
    QMutexLocker l(&answerHashMutex);
    if(status != Connection::Status::Connected)
    {
      ap->setResult(false, QVariant());
      return Answer(ap);
    }
    answerHash[id] = ap;
  }

  { // Add the request to the queue
    QMutexLocker l(&requestsQueueMutex);
    requestsQueue.push_back(req);
  }
  // Kick in the function that send requests
  QMetaObject::invokeMethod(this, "sendRequests", Qt::QueuedConnection);
  return Answer(ap);
}

void Connection::Private::stream(const QString& _service, int _id, QIODevice* _device)
{
  QMutexLocker l(&streamsMutex);
  Private::Stream stream;
  stream.id       = _id;
  stream.service  = _service;
  stream.device   = _device;
  streams.push_back(stream);
  // Kick in the function that send streams
  QMetaObject::invokeMethod(this, "sendStreams", Qt::QueuedConnection);
}


void Connection::Private::sendRequests()
{
  while(true)
  {
    Request msg;
    {
      QMutexLocker l(&requestsQueueMutex);
      if(requestsQueue.empty()) return;
      msg = requestsQueue.first();
      requestsQueue.removeFirst();
    }

    QJsonObject object;
    object["id"]        = msg.id;
    object["service"]   = msg.service;
    object["method"]    = msg.method;
    object["arguments"] = QJsonArray::fromVariantList(msg.arguments);
    sendJsonObject(object);
  }
}

void Connection::Private::sendInfo(const PeerInfo& _info)
{
  QJsonObject object;
  object.insert("name", _info.name());
  object.insert("id", _info.id());

  sendJsonObject(object);
}

void Connection::Private::sendStreams()
{
  QIODevice* device = model->device();
  while(true)
  {
    Stream msg;
    {
      QMutexLocker l(&streamsMutex);
      if(streams.empty()) return;
      msg = streams.first();
      streams.removeFirst();
    }
    if(not msg.device->isOpen())
    {
      msg.device->open(QIODevice::ReadOnly);
    }

    if(msg.device->isOpen())
    {
      const qint64 size = msg.device->size();

      QByteArray serviceNameArray = msg.service.toUtf8();

      QDataStream stream(device);
      stream << MessageHeader(size + BinaryHeader::F_END + serviceNameArray.size(), MessageHeader::T_STREAM) << BinaryHeader(msg.id, serviceNameArray.size());

      device->write(serviceNameArray);

      const int BLOCK_SIZE = 1000;
      QByteArray buffer(BLOCK_SIZE, Qt::Uninitialized);
      while(not msg.device->atEnd())
      {
        qint64 s = msg.device->read(buffer.data(), BLOCK_SIZE);
        device->write(buffer.data(), s);
      }
      msg.device->close();
    } else {
      qWarning() << "Failed to open device " << msg.device;
    }
    delete msg.device;
  }
}

MessageHeader Connection::Private::readMessageHeaderBlocking(int _attempts, int _delay)
{
  QIODevice* device = model->device();
  MessageHeader mh = readMessageHeader();

  for(int i = 0; i < _attempts and not mh.isValid(); ++i)
  {
    if(not device->waitForReadyRead(_delay))
    {
      // TODO emit an error
      return MessageHeader();
    }
    if(device->bytesAvailable() == 0)
    {
      // TODO emit an error
      qWarning() << device << " Time out: failed to connect" << device->bytesAvailable();
      return MessageHeader();
    }
    mh = readMessageHeader();
  }
  return mh;
}

bool Connection::Private::waitForMessageAvailable(const MessageHeader& _mh, int _delay)
{
  QIODevice* device = model->device();
  while(device->bytesAvailable() < (qint64)_mh.size())
  {
    if(not device->waitForReadyRead(_delay))
    {
      // TODO emit an error
      return false;
    }
  }
  return true;
}

QByteArray Connection::Private::readMessageBlocking(MessageHeader::Type _type)
{
  QIODevice* device = model->device();
  MessageHeader mh = readMessageHeaderBlocking(10, 60000);
  if(mh.isValid())
  {
    if(mh.type() == _type)
    {
      if(waitForMessageAvailable(mh, 60000))
      {
        return device->read(mh.size());
      }
    } else {
      if(waitForMessageAvailable(mh, 60000))
      {
        device->read(mh.size());
      }
    }
  }
  return QByteArray();
}

void Connection::Private::doStartConnection()
{
  status = Connection::Status::Connecting;
  model->initialise();
  QIODevice* device = model->device();
  if(device)
  {
    connect(device, SIGNAL(readChannelFinished()), this, SLOT(deviceIsClosing()));
    sendInfo(PeerInfo(peer->name(), peer->id(), QString()));

    // Parse greeting message
    {
      QByteArray greetingMessage = readMessageBlocking(MessageHeader::T_JSON);

      if(greetingMessage.isEmpty())
      {
        reportError("Failed to get a greeting message");
        close();
        status = Connection::Status::Disconnected;
        return;
      } else {
        QJsonDocument doc = QJsonDocument::fromJson(greetingMessage);
        remotePeerInfo = PeerInfo(doc.object().value("name").toString(), doc.object().value("id").toString(), model->address());
      }
    }

    // Now attempt authentication
    // 1) Send an authentication message
    QVariantMap authenticationRequest;
    {
      QVariantMap payload;
      {
        QMutexLocker l(&peer->d->authenticationMutex);
        authenticationRequest = peer->d->authentication->requestAuthentication(remotePeerInfo);
      }
      QJsonObject obj;
      obj["authenticate"] = QJsonValue::fromVariant(authenticationRequest);
      sendJsonObject(obj);
    }
    // 2) Wait for the authentication request
    {
      QByteArray authenticateMsg = readMessageBlocking(MessageHeader::T_JSON);
      if(authenticateMsg.isEmpty())
      {
        reportError("Failed to get an authentication message");
        close();
        status = Connection::Status::Disconnected;
        return;
      } else {
        QJsonDocument doc = QJsonDocument::fromJson(authenticateMsg);

        QVariantMap payload = doc.object()["authenticate"].toObject().toVariantMap();
        {
          QMutexLocker l(&peer->d->authenticationMutex);
          payload = peer->d->authentication->handleAuthenticationRequest(remotePeerInfo, payload);
        }
        QJsonObject obj;
        obj["authenticate"] = QJsonValue::fromVariant(payload);
        sendJsonObject(obj);
      }
    }
    // 3) Wait for the authentication answer
    {
      QByteArray authenticateMsg = readMessageBlocking(MessageHeader::T_JSON);
      if(authenticateMsg.isEmpty())
      {
        reportError("Failed to get an authentication answer");
        close();
        status = Connection::Status::Disconnected;
        return;
      } else {
        QJsonDocument doc = QJsonDocument::fromJson(authenticateMsg);

        QVariantMap payload = doc.object()["authenticate"].toObject().toVariantMap();
        {
          QMutexLocker l(&peer->d->authenticationMutex);
          if(peer->d->authentication->authenticate(remotePeerInfo, authenticationRequest, payload))
          {
            l.unlock();
            QJsonObject obj;
            obj["result"] = "authentication_accepted";
            sendJsonObject(obj);
          } else {
            l.unlock();
            reportError("Authentication rejected");
            emit(authenticationRejected());
            QJsonObject obj;
            obj["result"] = "authentication_rejected";
            sendJsonObject(obj);
            QTimer::singleShot(1000, this, [this]() { close(); }); // Make sure the connection is disconnected later so that the client can receive the authentication rejected token
            status = Connection::Status::Disconnected;
            return;
          }
        }
      }
    }
    // 4) wait for confirmation of authentication
    {
      QByteArray confirmationMsg = readMessageBlocking(MessageHeader::T_JSON);
      if(confirmationMsg.isEmpty())
      {
        reportError("Failed to get a confirmation message");
        close();
        status = Connection::Status::Disconnected;
        return;
      } else {
        QJsonDocument doc = QJsonDocument::fromJson(confirmationMsg);
        QString result_str    = doc.object()["result"].toString();
        if(result_str == "authentication_accepted")
        {
          emit(authenticationSuccessful());
        } else {
          reportError("Authentication failed");
          emit(authenticationFailed());
          close();
          status = Connection::Status::Disconnected;
          return;
        }
      }
    }

    {
      QMutexLocker l(&runWhenConnectionEstablishedMutex);
      status = Connection::Status::Connected;
      for(const std::function<void()>& f : runWhenConnectionEstablishedList)
      {
        QtConcurrent::run(threadPool, f);
      }
      runWhenConnectionEstablishedList.clear();
    }
    connect(device, SIGNAL(readyRead()), this, SLOT(readData()));
    emit(connectionReady());
  }
}

void Connection::Private::handleJson(const QByteArray& _data)
{
  QJsonDocument document = QJsonDocument::fromJson(_data);
  QJsonObject object = document.object();

  if(object.contains("service") and object.contains("method") and object.contains("id"))
  {
    Connection connection;
    connection.set(this);
    QtConcurrent::run(threadPool, [object, connection]() {
      const int id              = object.value("id").toVariant().toInt();
      const QString serviceName = object.value("service").toString();
      connection.d->peer->d->servicesMutex.lock();
      if(connection.d->peer->d->services.contains(serviceName))
      {
        AbstractService* service = connection.d->peer->d->services.value(serviceName);
        service->d->use.ref();
        connection.d->peer->d->servicesMutex.unlock();

        QVariantList args;
        if(object.contains("arguments"))
        {
          args = object.value("arguments").toVariant().toList();
        }
        QVariant ret = service->handleRequest(connection, object.value("method").toString(), args);
        service->d->use.deref();
        if(ret.canConvert<Error>())
        {
          QMetaObject::invokeMethod(connection.d, "sendError", Qt::QueuedConnection, Q_ARG(int, id), Q_ARG(pRpc::Error, qvariant_cast<Error>(ret)));
        } else {
          QJsonObject retObject;
          retObject.insert("id", id);
          retObject.insert("result", QJsonValue::fromVariant(ret));
          QMetaObject::invokeMethod(connection.d, "sendJsonObject", Qt::QueuedConnection, Q_ARG(QJsonObject, retObject));
        }
      } else {
        connection.d->peer->d->servicesMutex.unlock();
        QMetaObject::invokeMethod(connection.d, "sendError", Qt::QueuedConnection, Q_ARG(int, id), Q_ARG(pRpc::Error, Errors::UnknownService));
      }
    });

  } else if(object.contains("error") and object.contains("id"))
  {
    const int id              = object.value("id").toVariant().toInt();
    answerHashMutex.lock();
    if(answerHash.contains(id))
    {
      QSharedPointer<Answer::Private> ap = answerHash.value(id);
      answerHash.remove(id);
      answerHashMutex.unlock();
      QJsonObject errorObject = object.value("error").toObject();
      int code;
      QString msg;
      if(errorObject.contains("code"))
      {
        code = errorObject.value("code").toVariant().toInt();
      } else {
        code = EC_UNKNOWN;
      }
      msg = errorObject.value("message").toString();
      ap->setResult(false, QVariant::fromValue(Error(code, msg)));
    } else {
      answerHashMutex.unlock();
      qWarning() << "Received error for unknown id " << _data;
    }
  } else if(object.contains("result") and object.contains("id"))
  {
    const int id              = object.value("id").toVariant().toInt();
    answerHashMutex.lock();
    if(answerHash.contains(id))
    {
      QSharedPointer<Answer::Private> ap = answerHash.value(id);
      answerHash.remove(id);
      answerHashMutex.unlock();

      ap->setResult(true,  object.value("result").toVariant());
    } else {
      answerHashMutex.unlock();
      qWarning() << "Received answer for unknown id " << _data;
    }
  } else {
    // This is invalid stuff
    if(object.contains("id"))
    {
      sendError(object.value("id").toVariant().toInt(), Errors::InvalidJson);
      qWarning() << "Received invalid Json:" << _data;
    } else {
      qWarning() << "Received invalid Json, not even has an id:" << _data;
    }
  }
}


namespace pRpc {
  struct AbstractConnectionPrivateForwardDevice : public QIODevice
  {
    AbstractConnectionPrivateForwardDevice(QIODevice* _device, quint64 streamSize) : m_device(_device), m_pos(0), m_size(streamSize)
    {
    }
    inline qint64 maxAvailData() const
    {
      return m_size - m_pos;
    }
    bool atEnd() const override
    {
      return m_device->atEnd() or maxAvailData() == 0;
    }
    qint64 bytesAvailable() const override
    {
      return qMin(m_device->bytesAvailable(), maxAvailData());
    }
    qint64 size() const override
    {
      return m_size;
    }
    bool isSequential() const override
    {
      return true;
    }
    qint64 readData(char* data, qint64 maxlen) override
    {
      if(maxlen > maxAvailData()) maxlen = maxAvailData();
      int r = m_device->read(data, maxlen);
      m_pos += r;
      return r;
    }
    qint64 writeData(const char* data, qint64 len) override
    {
      Q_UNUSED(data);
      Q_UNUSED(len);
      qFatal("write is not supported");
    }
    bool waitForReadyRead(int msecs) override
    {
      return m_device->waitForReadyRead(msecs);
    }
    QIODevice* m_device;
    quint64 m_pos;
    const quint64 m_size;
  };

}

void Connection::Private::handleStream(int _size)
{
  QIODevice* device = model->device();
  if(_size < BinaryHeader::F_END)
  {
    qWarning() << "Missformed stream, not even the size of the BinaryHeader";
    device->read(_size);
    return;
  }

  for(int i = 0; i < 10 and device->bytesAvailable() < BinaryHeader::F_END; ++i)
  {
    device->waitForReadyRead(60000);
  }

  if(device->bytesAvailable() < BinaryHeader::F_END)
  {
    qWarning() << "time out";
    device->read(_size);
    return;
  }

  // Read header
  BinaryHeader bh;
  QByteArray data = device->read(BinaryHeader::F_END);
  QBuffer buff(&data);
  buff.open(QIODevice::ReadOnly);
  QDataStream stream(&buff);
  stream >> bh;

  for(int i = 0; i < 10 and (quint64)device->bytesAvailable() < bh.serviceNameSize(); ++i)
  {
    device->waitForReadyRead(60000);
  }

  QByteArray sn = device->read(bh.serviceNameSize());
  QString serviceName = QString::fromUtf8(sn);

  const quint64 streamSize = _size - bh.serviceNameSize() - BinaryHeader::F_END;

  peer->d->servicesMutex.lock();
  if(peer->d->services.contains(serviceName))
  {
    AbstractService* service = peer->d->services.value(serviceName);
    service->d->use.ref();
    peer->d->servicesMutex.unlock();

    AbstractConnectionPrivateForwardDevice acpfd(device, streamSize);
    acpfd.open(QIODevice::ReadOnly);

    Connection connection;
    connection.set(this);
    service->handleStream(connection, bh.id(), &acpfd);
    service->d->use.deref();

  } else {
    peer->d->servicesMutex.unlock();
    qWarning() <<"UnknownService " << serviceName;
    device->read(streamSize);
  }


}


void Connection::Private::sendJsonObject(const QJsonObject& _object)
{
  sendData(MessageHeader::T_JSON, QJsonDocument(_object).toJson(QJsonDocument::Compact));
}

void Connection::Private::sendError(int _id, const Error& _error)
{
  QJsonObject object;
  object.insert("id", _id);
  QJsonObject errorObject;
  errorObject.insert("code", _error.code());
  errorObject.insert("message", _error.message());
  object.insert("error", errorObject);
  sendJsonObject(object);
}

void Connection::Private::deviceIsClosing()
{
  status = Status::Disconnected;
  {
    QMutexLocker l(&runWhenConnectionEstablishedMutex);
    for(const std::function<void()>& f : runWhenDisconnected)
    {
      QtConcurrent::run(threadPool, f);
    }
    runWhenDisconnected.clear();

    {
      QMutexLocker l(&answerHashMutex);
      for(const QSharedPointer<Answer::Private>& ap : answerHash.values())
      {
        ap->setResult(false, QVariant());
      }
      answerHash.clear();
    }

  }
}

void Connection::Private::close()
{
  model->close();
}

void Connection::Private::closeAndDelete()
{
  Q_ASSERT(userCounter == 0);
  close();
  deleteLater();
}

void Connection::Private::reportError(const QString& _errorMsg)
{
  lastError = _errorMsg;
  emit(connectionError(_errorMsg));
}
