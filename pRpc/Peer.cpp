/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Peer_p.h"

#include <QtConcurrentRun>

#include "AbstractAuthentication.h"
#include "AbstractService_p.h"
#include "AbstractTransport_p.h"
#include "Connection_p.h"
#include "ConnectionsThread_p.h"
#include "Error.h"

using namespace pRpc;

ConnectionsThread* Peer::Private::getConnectionsThread()
{
  if(connectionsThreads.size() < maxConnectionsThreads)
  {
    ConnectionsThread* ct = new ConnectionsThread(self, &threadPool);
    ct->start();
    connectionsThreads.append(ct);
    return ct;
  } else {
    ConnectionsThread* bestCt = nullptr;
    int bestCtActiveThreads = std::numeric_limits<int>::max();
    for(ConnectionsThread* ct : connectionsThreads)
    {
      if(ct->activeConnections() < bestCtActiveThreads)
      {
        bestCt = ct;
        bestCtActiveThreads = ct->activeConnections();
      }
    }
    return bestCt;
  }
}

void Peer::Private::handleIncomingConnection(Connection _connection)
{
  getConnectionsThread()->startConnection(_connection);
  incomingConnections.append(_connection);
  self->incomingConnection(_connection);
}

Peer::Peer(const QString& _name, const QString& _id, AbstractTransport* _transport, AbstractAuthentication* _authentication) : d(new Private)
{
  d->self               = this;
  d->name               = _name;
  d->id                 = _id;
  d->transport          = _transport;
  d->transport->d->peer = this;
  d->authentication     = _authentication;
  d->maxConnectionsThreads  = qMin(2, QThread::idealThreadCount());
  
  qRegisterMetaType<pRpc::Error>("pRpc::Error");
}

Peer::~Peer()
{
  qDeleteAll(d->services.values());
  
  delete d->transport;
  delete d->authentication;
  delete d;
}

void Peer::addService(const QString& _name, AbstractService* _service)
{
  QMutexLocker l(&d->servicesMutex);
  d->services[_name] = _service;
}

void Peer::deleteService(AbstractService* _service)
{
  while(_service->d->use.loadRelaxed() != 0)
  {
    QThread::sleep(1);
  }
  delete _service;
}

void Peer::removeService(const QString& _name)
{
  AbstractService* service = 0;
  {
    QMutexLocker l(&d->servicesMutex);
    service = d->services[_name];
    d->services.remove(_name);    
  }
  if(service)
  {
    QtConcurrent::run(this, &Peer::deleteService, service);
  }
}

Connection Peer::connectTo(const QString& _address)
{
  Connection connection = d->transport->connectTo(_address);
  if(connection.isValid())
  {
    d->outgoingConnections.append(connection);
    d->getConnectionsThread()->startConnection(connection);
  }
  return connection;
}

QString Peer::name() const
{
  return d->name;
}

QString Peer::id() const
{
  return d->id;
}

Answer Peer::request(Connection _connection, const QString& _service, const QString& _method, const QVariantList& _parameters)
{
  return _connection.d->request(_service, _method, _parameters);
}

void Peer::stream(Connection _connection, const QString& _service, int _id, QIODevice* _device)
{
  Q_ASSERT(not _device->isSequential());
  _connection.d->stream(_service, _id, _device);
}

Connection Peer::connectionTo(const QString& _id) const
{
  foreach(const Connection& connection, d->incomingConnections)
  {
    if(connection.remotePeerInfo().id() == _id) return connection;
  }
  foreach(const Connection& connection, d->outgoingConnections)
  {
    if(connection.remotePeerInfo().id() == _id) return connection;
  }
  return 0;
}

#include "moc_Peer.cpp"
