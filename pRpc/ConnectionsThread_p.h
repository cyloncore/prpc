/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QThread>

class QThreadPool;

namespace pRpc
{
  class Connection;
  class Peer;
  class ConnectionsThread : public QThread
  {
    Q_OBJECT
  public:
    ConnectionsThread(Peer* _peer, QThreadPool* _threadPool);
    ~ConnectionsThread();
    int activeConnections();
    void startConnection(Connection _connection);
  private:
    struct Private;
    Private* const d;
  };
}
