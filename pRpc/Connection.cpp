/*
 *  Copyright (c) 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Connection_p.h"

using namespace pRpc;

Connection::Connection(AbstractConnectionModel* _model) : d(nullptr)
{
  if(_model)
  {
    set(new Private(_model));
  }
}

Connection::Connection() : d(nullptr)
{
}

Connection::Connection(const Connection& _rhs) : d(nullptr)
{
  set(_rhs.d);
}

Connection & Connection::operator=(const Connection& _rhs)
{
  deref();
  set(_rhs.d);
  return *this;
}

Connection::~Connection()
{
  deref();
}

void Connection::clear()
{
  deref();
}

bool Connection::isValid() const
{
  return d;
}

void Connection::connect(const char* signal, const QObject* receiver, const char* member)
{
  QObject::connect(d, signal, receiver, member);
}

void Connection::onEstablishedConnection(const std::function<void()>& _functor)
{
  QMutexLocker l(&d->runWhenConnectionEstablishedMutex);
  
  if(d->status != Connection::Status::Connected)
  {
    d->runWhenConnectionEstablishedList.append(_functor);
  } else {
    l.unlock();
    _functor();
  }
}

void Connection::onDisconnection(const std::function<void ()>& _functor)
{
  QMutexLocker l(&d->runWhenConnectionEstablishedMutex);
  
  if(d->status != Connection::Status::Connected)
  {
    d->runWhenDisconnected.append(_functor);
  } else {
    l.unlock();
    _functor();
  }
}

PeerInfo Connection::remotePeerInfo() const
{
  return d->remotePeerInfo;
}

Peer* Connection::peer() const
{
  return d->peer;
}


void Connection::requestClose()
{
  QMetaObject::invokeMethod(d, "close", Qt::QueuedConnection);
}


Connection::Status Connection::status() const
{
  return d->status;
}

void Connection::deref()
{
  if(d)
  {
    d->userCounter--;
    if(d->userCounter == 0)
    {
      QMetaObject::invokeMethod(d, "closeAndDelete", Qt::QueuedConnection);
    }
    d = nullptr;
  }
}

void Connection::set(Connection::Private* _d)
{
  if(_d)
  {
    _d->userCounter++;
    d = _d;
  }
}

bool Connection::operator!=(const Connection& _rhs) const
{
  return d == _rhs.d;
}
