/*
 *  Copyright (c) 2013,2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Connection.h"

#include <QMutex>
#include <QObject>

#include "Answer_p.h"
#include "PeerInfo.h"
#include "MessageHeader.h"

class QThreadPool;

namespace pRpc
{
  class Error;
  struct Connection::Private : QObject
  {
    Q_OBJECT
  public:
    Private(AbstractConnectionModel* _model);

    Peer* peer;
    AbstractConnectionModel* model;
    Status status;
    PeerInfo remotePeerInfo;
    
    MessageHeader readMessageHeader();
    MessageHeader readMessageHeaderBlocking(int _attempts, int _delay);
    QByteArray readMessageBlocking(MessageHeader::Type _type);
    
    bool waitForMessageAvailable(const MessageHeader& _mh, int _delay);
    void sendData(MessageHeader::Type _type, const QByteArray& _data);
    Q_INVOKABLE void sendJsonObject(const QJsonObject& _object);
    Q_INVOKABLE void sendError(int _id, const pRpc::Error& _error);
    
    struct Request
    {
      int id;
      QString service, method;
      QVariantList arguments;
    };
    int nextRequestId;
    
    QMutex runWhenConnectionEstablishedMutex;
    QList<std::function<void()>> runWhenConnectionEstablishedList;
    QList<std::function<void()>> runWhenDisconnected;
    
    QMutex requestsQueueMutex;
    QList<Request> requestsQueue;
    
    QMutex answerHashMutex;
    QHash<int, QSharedPointer<Answer::Private> > answerHash;
    
    struct Stream
    {
      int id;
      QString service;
      QIODevice* device;
    };
    
    QMutex streamsMutex;
    QList<Stream> streams;
    
    MessageHeader currentHeader;
    
    QString lastError;
    
    QThreadPool* threadPool = nullptr;
    
    QAtomicInt userCounter;

    void sendInfo(const PeerInfo& _info);
    Answer request(const QString& _service, const QString& _method, const QVariantList& _parameters);
    void stream(const QString& _service, int _id, QIODevice* _device);
  private:
    void handleJson(const QByteArray& _data);
    void handleStream(int _size);
  private slots:
    void reportError(const QString& _errorMsg);
  signals:
    void connectionError(const QString& _errorMsg);
    void connected();
    void disconnected();
    void connectionReady();
    void authenticationSuccessful();
    // Emited when the server rejected our authentication
    void authenticationFailed();
    // Emited when we reject a client authentication
    void authenticationRejected();
  public slots:
    void deviceIsClosing();
    void readData();
    void sendRequests();
    void sendStreams();
    void close();
    void closeAndDelete();
    void doStartConnection();
  };
}
