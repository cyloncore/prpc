/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PRPC_PEER_H_
#define _PRPC_PEER_H_

#include <QHash>
#include <QObject>
#include <QVariant>

#include "Connection.h"

class QIODevice;

namespace pRpc
{

  class AbstractAuthentication;
  class Answer;
  class AbstractService;
  class AbstractTransport;
  class ConnectionsThread;
  class PeerInfo;
  
  class Peer : public QObject
  {
    Q_OBJECT
    friend class AbstractTransport;
    friend class ConnectionsThread;
    friend class Connection::Private;
  public:
    /**
     * @param _port port value, if -1, it will automatically select
     */
    Peer(const QString& _name, const QString& _id, AbstractTransport* _transport, AbstractAuthentication* _authentication);
    ~Peer();
    void addService(const QString& _name, AbstractService* _service);
    template<typename _T_>
    void addService(_T_* _service)
    {
      addService(_T_::name(), _service);
    }
    void removeService(const QString& _name);
    Answer request(Connection _connection, const QString& _service, const QString& _method, const QVariantList& _parameters);
    void stream(Connection _connection, const QString& _service, int _id, QIODevice* _device);
    Connection connectTo(const QString& _address);
    /**
     * @return the connection to the peer with the given id. If connected, otherwise, it returns 0.
     */
    Connection connectionTo(const QString& _id) const;
    QString name() const;
    QString id() const;
  signals:
    void incomingConnection(Connection);
  private:
    void deleteService(AbstractService* _service);
  private:
    struct Private;
    Private* d;
  };
}

#endif
