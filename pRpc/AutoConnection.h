#pragma once

#include <QObject>

namespace pRpc
{
  class Connection;
  class Peer;
  class AutoConnection : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QStringList serverAddresses READ serverAddresses WRITE setServerAddresses NOTIFY serverAddressesChanged)
  public:
    AutoConnection(Peer* _peer, QObject* _parent = nullptr);
    ~AutoConnection();
    bool isConnected() const;
    QStringList serverAddresses() const;
    void setServerAddresses(const QStringList& _sA);
  signals:
    void connectedChanged();
    void serverAddressesChanged();
  private:
    void initiateConnection();
  private slots:
    void connected();
    void disconnected();
  public:
    pRpc::Connection connection() const;
  private:
    struct Private;
    Private* const d;
  };
}
