/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "BinaryHeader.h"

#include <QDataStream>

using namespace pRpc;

struct BinaryHeader::Private : public QSharedData
{
  quint64 id, serviceNameSize;
  bool valid;
};

BinaryHeader::BinaryHeader() : d(new Private)
{
  d->valid = false;
}

BinaryHeader::BinaryHeader(quint64 _id, quint64 _serviceNameSize) : d(new Private)
{
  d->id     = _id;
  d->serviceNameSize = _serviceNameSize;
  d->valid  = true;
}

BinaryHeader::BinaryHeader(const BinaryHeader& _rhs) : d(_rhs.d)
{

}

BinaryHeader& BinaryHeader::operator=(const BinaryHeader& _rhs)
{
  d = _rhs.d;
  return *this;
}

BinaryHeader::~BinaryHeader()
{

}

quint64 BinaryHeader::id() const
{
  return d->id;
}

quint64 BinaryHeader::serviceNameSize() const
{
  return d->serviceNameSize;
}

bool BinaryHeader::isValid() const
{
  return d->valid;
}

QDataStream& pRpc::operator<<(QDataStream& stream, const BinaryHeader& _header)
{
  stream << _header.id() << _header.serviceNameSize();
  return stream;
}

QDataStream& pRpc::operator>>(QDataStream& stream, BinaryHeader& _header)
{
  _header.d.detach();
  stream >> _header.d->id >> _header.d->serviceNameSize;
  _header.d->valid = true;
  return stream;
}
