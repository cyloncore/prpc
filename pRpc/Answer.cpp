/*
 *  Copyright (c) 2013, 2017, 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Answer_p.h"

#include "Error.h"

using namespace pRpc;

Answer::Answer(QSharedPointer<Private> _d) : d(_d)
{
  
}

Answer::Answer() : d(new Private(false))
{
  
}

Answer::Answer(const Answer& _rhs) : d(_rhs.d)
{
  
}

Answer& Answer::operator=(const Answer& _rhs)
{
  d = _rhs.d;
  return *this;
}

Answer::~Answer()
{
  
}

bool Answer::isAvailable() const
{
  return d->isAvailable;
}

bool Answer::waitForAvailable(int _time) const
{
  d->waitAvailableMutex.lock();
  if(not d->validRequest)
  {
    return false;
  }
  if(d->isAvailable)
  {
    d->waitAvailableMutex.unlock();
    return true;
  }
  bool cond = d->waitAvailableCondition.wait(&d->waitAvailableMutex, (_time == -1) ? ULONG_MAX : _time);
  d->waitAvailableMutex.unlock();
  return cond;
}

void Answer::executeOnAvailable(const std::function<void (bool, const QVariant &)>& _function)
{
  QMutexLocker l(&d->waitAvailableMutex);
  if(d->isAvailable)
  {
    _function(d->isSuccessfull, d->result);
  } else {
    d->functions.append(_function);
  }
}

bool Answer::isSuccessful() const
{
  return d->isSuccessfull;
}

QVariant Answer::result() const
{
  return d->result;
}

Error pRpc::Answer::error() const
{
  return d->result.value<pRpc::Error>();
}
