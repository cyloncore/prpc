/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractService_p.h"

#include "Errors.h"

#include <QVariant>

using namespace pRpc;

AbstractService::AbstractService() : d(new Private)
{

}

AbstractService::~AbstractService()
{
  delete d;
}

QVariant AbstractService::handleRequest(Connection _connection, const QString& _method, const QVariantList& _arguments)
{
  Q_UNUSED(_connection)
  Q_UNUSED(_method)
  Q_UNUSED(_arguments)
  return QVariant::fromValue(Errors::UnknownMethod);
}

