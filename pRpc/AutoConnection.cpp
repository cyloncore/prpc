#include "AutoConnection.h"

#include <QTimer>

#include "Connection.h"
#include "Peer.h"

using namespace pRpc;

struct AutoConnection::Private
{
  Connection connection;
  Peer* peer;
  QStringList serverAddresses;
  int index = 0;
};

AutoConnection::AutoConnection(Peer* _peer, QObject* _parent) : QObject(_parent), d(new Private)
{
  d->peer = _peer;
}

AutoConnection::~AutoConnection()
{
}

bool AutoConnection::isConnected() const
{
  return d->connection.isValid() and d->connection.status() == Connection::Status::Connected;
}

Connection AutoConnection::connection() const
{
  return d->connection;
}

QStringList AutoConnection::serverAddresses() const
{
  return d->serverAddresses;
}

void AutoConnection::setServerAddresses(const QStringList& _sA)
{
  d->serverAddresses = _sA;
  emit(serverAddressesChanged());
  if(d->connection.isValid()) d->connection.requestClose();
  d->index = 0;
  initiateConnection();
}

void AutoConnection::initiateConnection()
{
  if(not d->peer or d->serverAddresses.isEmpty()) return;
  if(d->index >= d->serverAddresses.size()) d->index = 0;
  if(d->connection.isValid() and d->connection.status() != Connection::Status::Disconnected) return;
  d->connection.clear();
  d->connection = d->peer->connectTo(d->serverAddresses[d->index]);
  d->connection.connect(SIGNAL(connectionReady()), this, SLOT(connected()));
  d->connection.connect(SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void AutoConnection::connected()
{
  emit(connectedChanged());
}

void AutoConnection::disconnected()
{
  emit(connectedChanged());
  ++d->index;
  QTimer::singleShot(100, [this]() { initiateConnection(); });
}

#include "moc_AutoConnection.cpp"
