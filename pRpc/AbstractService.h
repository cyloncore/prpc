/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QVariant>

#include "Connection.h"

class QIODevice;

namespace pRpc
{
  class Peer;
  class AbstractService
  {
    friend class Connection::Private;
    friend class Peer;
  public:
    AbstractService();
    virtual ~AbstractService();
    
    virtual QVariant handleRequest(Connection _connection, const QString& _method, const QVariantList& _arguments);
    virtual void handleStream(Connection _connection, int _id, QIODevice* _iodevice) = 0;
    virtual void handleData(Connection _connection, int _id, const QByteArray& _data) = 0;
    
  private:
    struct Private;
    Private* const d;
  };
}
