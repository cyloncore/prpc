/*
 *  Copyright (c) 2018 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ServerPasswordAuthentication.h"

#include <QDebug>
#include <QRandomGenerator>

#include "PeerInfo.h"

using namespace pRpc;

struct ServerPasswordAuthentication::Private
{
  QCryptographicHash::Algorithm algorithm;
};

ServerPasswordAuthentication::ServerPasswordAuthentication(QCryptographicHash::Algorithm _hashAlgorithm) : d(new Private)
{
  d->algorithm = _hashAlgorithm;
}

ServerPasswordAuthentication::~ServerPasswordAuthentication()
{
}

#define HASH_TYPE(_HASH_ALG_)           \
  case QCryptographicHash::_HASH_ALG_:  \
    payload["hash"] = # _HASH_ALG_;     \
    break;

QVariantMap ServerPasswordAuthentication::requestAuthentication(const PeerInfo& _info)
{
  QVariantMap payload;
  switch(d->algorithm)
  {
    HASH_TYPE(Sha224)
    HASH_TYPE(Sha256)
    HASH_TYPE(Sha512)
    HASH_TYPE(Sha3_224)
    HASH_TYPE(Sha3_256)
    HASH_TYPE(Sha3_384)
    HASH_TYPE(Sha3_512)
    default:
      qFatal("Only SHA hashes are supported");
  }
  QByteArray salt;
  salt.resize(64*sizeof(quint32));
  quint32* salt_data = reinterpret_cast<quint32*>(salt.data());
  QRandomGenerator::system()->generate(salt_data, salt_data + 64);
  payload["salt"] = QString::fromLatin1(salt.toBase64());
  
  return payload;
}

QVariantMap ServerPasswordAuthentication::handleAuthenticationRequest(const PeerInfo& _info, const QVariantMap& _payload)
{
  return QVariantMap();
}

bool ServerPasswordAuthentication::authenticate(const PeerInfo& _info, const QVariantMap& _authenticationRequest, const QVariantMap& _payload)
{
  QByteArray salt               = QByteArray::fromBase64(_authenticationRequest["salt"].toString().toLatin1());
  QString username              = _info.id();
  QByteArray saltedPasswordHash = QByteArray::fromBase64(_payload["password"].toString().toLatin1());
  qDebug() << "SPA1:" << username.isEmpty();
  qDebug() << _authenticationRequest << _payload;
  if(username.isEmpty()) return false;
  
  QByteArray serverPassword     = password(username, d->algorithm);

  QByteArray saltedHash;
  QCryptographicHash qch(d->algorithm);
  qch.addData(salt);
  qch.addData(serverPassword);
  
  qDebug() << "SPA2:" << saltedPasswordHash << qch.result();
  
  return saltedPasswordHash == qch.result();
}
